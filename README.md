food
====

A home recipe site built with Jekyll.

Repo Organization
================

Root contains configuration files for Jekyll, documentation, and the homepage of the site.

`recipes` contains folders of markdown files that become content on this site. Each folder must appear
in the list of `collections` in `_config.yml` in order to be displayed.

`_site` will contain the complete site built by Jekyll.

Installing
==========

You'll need a recent version of ruby installed.

If you use asdf, install the [ruby plugin](https://github.com/asdf-vm/asdf-ruby).
Then run `asdf install`

Then, to install all dependencies:

```
bundle install
```

Building and Serving
====================

To serve on port 4000 and rebuild while adding content or developing:

```
bundle exec jekyll serve --livereload
```

Publishing
==========

Commit to main and push to Gitlab; CI will deploy to food.wbushey.com.

Favicon
=======

The favicon is from https://www.freefavicon.com/freefavicons/objects/iconinfo/cooking-pot----152-220844.html
