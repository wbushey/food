---
title: Multigrain Bread
ingredients:
  millet flour: 1 cup
  tapioca starch: 1 cup
  blanched almond flour: 1/2 cup
  brown teff or amaranth flour: 1/2 cup
  sorghum flour: 1/4 cup
  flax meal: 1/4 cup
  xanthan gum: 2 3/4 teaspoons
  sea salt: 1 1/2 teaspoons
  eggs: 3
  olive oil: 3 tablespoon
  unsulfured molasses: 1 tablespoon
  apple cider vinegar: 1 teaspoon
  hot water: 1 1/4 cup 
  honey: 2 tablespoons
  dry active yeast: 2 1/2 teaspoons
tags:
  - gluten-free
time: 120 minutes
servings: 20
---

1. In a small mixing bowl, combine the honey and the hot water (110-115 degrees F). Sprinkle in the yeast (don't use instant) and give it a quick stir to combine. Allow to proof for 7 minutes (set a timer!) – NO more, NO less time. Make sure you have the other wet and dry ingredients mixed and ready to go when the 7 minutes are up!
2. Using a heavy duty mixer with a paddle attachment, combine the dry ingredients.
3. In a separate mixing bowl, whisk together the eggs, oil, molasses, and vinegar.
4. When the yeast is done proofing, add the wet ingredients to the dry. Stir until it’s a little paste-like, then slowly add the yeast mixture. Using your mixer’s low speed setting, mix for about 30 seconds. Scrape the sides of the bowl then mix on medium for 2 – 3 minutes or until the dough is smooth. (You may need to stop your mixer and scrape the sides of your bowl a few more times.)
5. Pour dough into a parchment lined and well greased 9 x 5? metal bread pan (the only pan I recommend for this recipe is a metal one, you will not have the same results using other pans) and cover with plastic wrap. Allow to rise for 45 minutes to an hour (Check the loaf 30 minutes into rising. When the dough is close to hitting the plastic wrap, remove it; allow the dough to rise the remaining time uncovered.) When bread is finished rising, bake in a preheated 375 degrees (F) oven for about 30 minutes.
6. Remove loaf from pan and allow it to cool on a wire rack. Allow the loaf to completely cool before slicing (if you can stand to leave it alone for that long!).

[Source](http://www.allergyfreealaska.com/2012/03/12/gluten-rice-free-multigrain-bread/)
