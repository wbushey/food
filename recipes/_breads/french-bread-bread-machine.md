---
title: French Bread - Bread Machine
ingredients:
  flour: 2 cups
  yeast: 1 tsp instant
  salt: 3/4 tsp
  water: 2/3 cup
  sugar: t tbsp
tags:
  - vegetarian
time: 4 hours
servings: 4
---

1. Plugin Bread Machine, insert kneading paddle into baking pan
2. Microwave water to 110F - 120F - approximately 45 seconds
3. Mix water, sugar, and yeast
4. Combine flour and salt in machine baking pan
5. Mix in water with flour; mix with handle of wooden spoon until all ingredients are just combined
6. Start French Bread program (program 5), choose 1 lb light bake
7. After ~30 minutes the mixing/kneading cycle will finish; carefully removing the kneading paddle
8. Let French Bread program finish
