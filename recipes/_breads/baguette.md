---
title: Baguette
ingredients:
  flour: 225 grams
  yeast: 3/16 tsp instant, or 1/4 tsp dry active
  salt: 2/3 tsp
  water: 183 ml
  sugar: pinch, only if using dry active yeast
tags:
  - vegetarian
time: 9 hours
servings: 4
---

## Make Dough

### With Dry Active Yeast

1. Microwave 183 ml water to 110F - 120F - approximately 45 seconds
2. Mix in sugar with a fork
3. Mix in 1/4 tsp dry active yeast with a fork
4. Cover and let proof for 5 - 10 minutes
5. Mix flour and salt in bowl
6. Mix in water/yeast. Stir with handle of wooden spoon until the dough is sticky/sticks to the handle. Use fork to separate the dough from the spoon handle.
7. Cover bowl and let dough rise for 8 - 10 hours

### With Instant Yeast

1. Mix flour and salt in bowl
2. Mix in 3/16 tsp instant yeast
3. Mix in water. Stir with handle of wooden spoon until the dough is sticky/sticks to the handle. Use fork to separate the dough from the spoon handle.
4. Cover bowl and let dough rise for 8 - 10 hours


## Baking

1. Preheat oven to 480F. Place pan full of water and a towel on bottom of the oven.
2. Transfer dough from bowl to parchment paper. Shape dough into a baguette. Be careful not to compress the dough or disturb air in the dough.
3. Bake dough for 23 minutes; until a golden brown crust develops.


Based on [this video](https://www.youtube.com/watch?v=Z-husjZkxHw)