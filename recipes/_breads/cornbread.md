---
title: Cornbread
ingredients: 
  certified gluten-free cornmeal: 1 cup
  rice flour: 1/2 cup white or brown
  cornstarch: 1/2 cup
  baking powder: 1 Tablespoon
  salt: 1/2 teaspoon
  sugar: 1/4 cup
  milk: 1 cup any kind
  oil: 1/3 cup 
  eggs: 2 (or equivalent replacement)
tags:
  - gluten-free
time: 50 minutes
servings: 9
---

1. Preheat oven to 400F.
2. Grease an 8-inch square glass baking dish (or similar sized dish of your choice, or a cast iron skillet, if you have one.)
3. Place the baking dish or skillet in the oven as the oven preheats while you mix the bread. A hot skillet before you pour in the batter ensures a crisp outer crust, which is a must with cornbread!)
4. In a mixing bowl, whisk dry ingredients to blend.
5. Add milk, oil, and eggs; stir until batter is completely smooth.
6. Carefully remove hot pan from the oven using an oven mitt, and pour the batter into the hot pan (this seals a crisp edge on your cornbread).
7. Return the pan to oven and bake 20-25 minutes, or until cornbread is golden brown on top and deep golden brown around the edges.
8. Cut into squares and serve hot from the oven either on its own or with a drizzle of honey.

[Source](http://www.glutenfreegigi.com/the-best-gluten-free-cornbread/)
