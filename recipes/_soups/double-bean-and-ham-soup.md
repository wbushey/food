---
title: Double Bean and Ham Soup
ingredients:
  ham steak:          1
  tomatoes - diced :  1 (14.5 oz) can
  pork and beans:     1 (16 oz) can
  dry navy beans:     16 oz
  chicken broth:      2 cups
  carrots:            2
  celery:             2 stalks
  corn:               1 cup
  garlic:             2 cloves
  onion:              1
  water:              2 cups
  salt:               to taste
  pepper:             to taste
tags:
  - pork
  - gluten-free
time:             90 minutes
---

1. Make chicken broth, if needed.
2. Chop garlic and onion, sauté in pressure cooker.
3. Chop carrots and celery.
4. Put all ingredients, except salt and pepper, into pressure cooker. Include liquid in canned ingredients.
5. Cook at high pressure for 45 minutes.
6. Allow for natural pressure release.
6. When pressure release is finished, stir in salt and pepper. 

[Source](http://allrecipes.com/recipe/260073/instant-pot-double-bean-and-ham-soup/)
