---
title: Butternut Squash Soup
ingredients:
  butternut squash: 1 medium (4 cups)
  onion: 1 small (2/3 cup)
  green apple: 1 medium
  potatoes or sweet potatoes: 2 medium (1 1/2 cups)
  chicken broth: 3 cups
  oil or butter: 1 Tablespoon
  fresh rosemary: 1/2 tsp fresh or 1/4 tsp dried
  salt and pepper: dash
tags:
  - vegetarian
  - gluten-free
time: 20 minutes
servings: 6
---

1. Prepare the vegetables: peel and cube squash and (sweet) potato, peel and chop apple, chop onion.
2. Heat the oil or butter in the bottom of a pot. Add the onion and sauté until softened.
3. Add the chicken broth and heat to boiling.
4. Stir in the vegetables and rosemary. Reduce heat, cover, and simmer until the vegetables are tender. The amount of time will depend on how small you cut them.
5. Add salt and pepper to taste.
6. Blend the soup with an immersion blender, or by adding it in batches to a stand blender. The soup should be smooth with no chunks in it.

Notes: I saute the onion and then add the squash, apple, and potato; the browning that occurs in the pan before you add the broth adds flavor!

[Source](http://glutenfreehomemaker.com/butternut-squash-soup/)
