---
title: Thai Coconut Curry Chicken Soup
ingredients:
  Coconut Milk: 1 can (14 oz)
  Lite Coconut Milk: 1 can (14 oz)
  Chicken broth: 2 Cans (14 oz)
  ginger: 1 Tbsp fresh grated
  Lemongrass: 2 stalks
  mushrooms: 2 cups
  lime juice: 1/2 lime squeezed
  Thai fish sauce: 2 Tbsp
  sugar: 1 Tbsp
  Thai chili paste: 1 Tbsp + 1 tsp
  cayenne pepper: 1/2 tsp
  curry powder: 1/2 tsp.
  fresh basil: 1/4 cup chopped
  fresh cilantro: 1/4 cup chopped
  Jasmine rice: 2 cups fully cooked
  chicken breasts: 2 fully cooked
  Salt: dash
tags:
  - poultry
  - gluten-free
time:
servings:
---

1. Slice mushrooms, grate ginger, chop basil and cilantro.
2. In a medium saucepan, cook Jasmine rice. In a large pot, combine coconut milks, chicken broth, ginger, and lemongrass. Bring to a boil over medium high heat. 
3. Add cooked chicken, mushrooms, fish sauce, sugar, chili paste, curry powder and cayenne pepper. Reduce heat and simmer for 10-15 minutes. 
4. Add lime, chopped cilantro and cooked jasmine rice. Salt to taste and garnish with freshly cut basil.

[Source](http://www.glutenfreeyummy.com/thai-coconut-curry-soup/)
