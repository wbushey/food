---
title: Chicken Pot Pie Soup
ingredients:
  chicken thighs: 1.5lbs boneless skinless
  red potatoes: 5 medium
  carrots: 5
  celery: 4 stalks
  corn: 2 ears -or- 1 bag frozen
  green beans: 1/2lb fresh -or- 1 bag frozen
  peas: 1 frozen bag
  chicken broth: 32oz
  cream of chicken soup - gluten-free: 24oz
  black pepper: 1tbsp
  garlic salt: 2tsp
  sage: optional
  thyme: optional
  rosemary: optional
tags:
  - poultry
  - gluten-free
time: 5.5 hours
servings: 10
---

1. Chop potatoes into medium chunks. Cut carrots and celery into ~1 inch pieces. Place in slow cooker.
2. Chop chicken into medium chunks. Add to slow cooker.
3. Add cream of chicken soup, chicken broth, pepper, garlic salt, and any herbs to slow cooker. Mix.
4. Slow cook on low for 4 hours.
5. While slow cooking, prepare remaining vegetables as needed.
6. If using fresh green beans, after 3 hours 30 minutes of cooking, add green beans.
6. Add remaining vegetables to slow cooker. Cook on low for 1 hour.
