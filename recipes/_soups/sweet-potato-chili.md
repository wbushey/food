---
title: Sweet Potato Black Bean Chili
ingredients:
  sweet potatoes: 2 lbs
  dried chipotle pepper: 1/2 teaspoon ground 
  salt : 1/2 teaspoon
  olive oil: 2 tablespoons
  onion: 1
  garlic: 4 cloves
  red bell pepper: 1
  jalapeno pepper: 1
  ancho chile powder: 2 tablespoons
  ground cumin: 1 tablespoon 
  dried oregano: 1/4 teaspoon 
  diced tomatoes: 1 (28 ounce) can
  water: 1 cup, or more
  cornmeal: 1 tablespoon 
  salt: 1 teaspoon
  white sugar: 1 teaspoon 
  unsweetened cocoa powder: 1 teaspoon
  black beans: 2 (15 ounce) cans
  cayenne pepper: 1 pinch
  sour cream: 1/2 cup
  fresh cilantro: 1/4 cup chopped
tags:
  - vegetarian
  - gluten-free
time: 90 minutes
servings: 4
---

0. Prep: peel and cube sweet potatoes, dice onion, pepper, jalapeno, mince garlic.
1. Preheat oven to 400 degrees F (200 degrees C). Line a baking sheet with parchment paper or a silicone baking mat.
2. Combine sweet potatoes, chipotle pepper, 1/2 teaspoon salt, and 1 tablespoon olive oil in a large bowl and toss to coat. Spread sweet potatoes on the prepared baking sheet in a single layer.
3. Roast sweet potatoes in the preheated oven until the outside is crunchy and inside is tender, 20 to 25 minutes. Allow to cool to room temperature.
4. Cook and stir remaining 1 tablespoon olive oil, onion, garlic, red bell pepper, jalapeno pepper, ancho chile powder, cumin, and dried oregano together in a large pot or Dutch oven over medium heat. Cook and stir until onion is softened, about 5 minutes.
5. Pour tomatoes and water into the onion mixture and bring to a simmer. Add cornmeal, 1 teaspoon salt, sugar, and cocoa powder. Bring to a simmer, stirring constantly, reduce heat to low and simmer for 30 minutes.
6. Stir rinsed black beans and cooled sweet potatoes into the onion-tomato mixture. Add more water if mixture is too thick. Simmer until heated through, about 15 minutes. Season with salt and cayenne pepper to taste. Serve topped with sour cream and cilantro.

[Source](http://allrecipes.com/recipe/229730/sweet-potato-and-black-bean-chili/)
