---
title: Tomato Soup
ingredients:
  tomatoes: 6 ripe
  carrots: 3
  green or red bell pepper: 1
  yellow onion: 1
  garlic: 5 cloves
  fresh basil : 6-8 leaves
  chicken or veg broth : 2 cups
  Paprika : 2 tsp
  Smoked Serrano Chili Powder: Dash
  dried thyme: Dash
  salt and pepper: dash
  milk: 1/2 cup
  butter: dab
  Olive oil: some
tags:
  - vegetarian
  - gluten-free
time: 30-40 minutes
servings: 2-3
---

0. Prep: cut tomatoes, peppers, onions into chunks; peel and slice carrots into discs; halve the garlic cloves.
1. Add a few tablespoons of olive oil or coconut oil into a deep frying pan, and heat on high. When hot, toss in the garlic and onion and sauté until lightly browned. Then add in the carrot and sauté until lightly browned.
2. Add in another tablespoon of olive oil, along with the bell pepper and tomatoes. Cook on medium fire until tomatoes are softened. Turn heat off and let cool while completing next step.
3. In a large pot, combine the chicken broth, paprika, smoked serrano, thyme, and black pepper. Heat on low.
4. Add the cooked veggies into a blender, along with the fresh basil leaves. Blend on low speed to desired texture.
5. Add the blended mixture into the large pot, stir well with the broth, and cook on medium fire until soup thickens to desired texture. Then stir in butter and milk, add in more salt if needed, and cook a few more minutes.
6. Serve hot, and garnish with shredded fresh basil leaves and freshly ground black pepper. Enjoy with a side of toast or a grilled cheese sandwich.

[Source](http://blog.seasonwithspice.com/2012/08/homemade-tomato-basil-soup-recipe.html)

