---
title: Mushroom, Potato, and Wild Rice Soup
ingredients:
  mushrooms: 1 lb
  potatos: 2 yukon gold or other medium-starch
  wild rice: 2/3 cup uncooked
  leeks: 1.5 cups
  garlic: 3 cloves
  broth: 4 cups vegetable or chicken
  half and half: 1 cup
  tomato paste: 3 tbsp
  butter: 3 tbsp unsalted
  dill: 1/4 cups fresh
  smoked paprika: 1 tsp
  red pepper flakes: 1/4 tsp
  caraway seeds: 1/4 tsp
  thyme: small handful sprigs
  rosemary: two 4in sprigs
  black pepper:
  salt:
tags:
  - vegetarian
  - gluten-free
vegetarian: true
time: 90 minutes
servings: 6
---

1. Make broth if needed.
2. Cook wild rice in pressure cooker - approximately 30 minutes.
3. Slice leeks crosswise into 1/8 in slices, slice mushrooms, finely chop garlic, chop potatos into small cubes, chop dill. Gather spices.
4. Heat large skillet with olive oil over medium-high heat. Cook mushrooms in an even layer, in batches if needed. Season with chile flakes, salt, and pepper. Cook 6 minutes (until browned and slightly crisp at the edges), then flip and cook other side for 4 minutes. When done, scrape mushrooms into a bowl and set aside.
5. Reduce heat to medium, add butter and leek, season with salt and pepper. Cook about 10 minutes, until leek slices are soft and fragrant. Add garlic and cook for another minute or two.
6. Add tomato paste, smoked paprika, and caraway seeds. Cook, stirring and scraping, about 5 minutes, until tomato paste has darkened and thickened a bit.
7. Add broth, potatos, thyme, and rosemary. Bring to a simmer, cook about 20 minutes - until potato pices are tender and easily crushable.
8. Add cream, wild rice, and mushrooms. Simmer gently about 15 minutes to thicken and cook off raw cream flavor.
9. Taste and season with salt and pepper. Remove thyme and rosemary stems. Add dill.



[Source](https://www.nigella.com/recipes/guests/creamy-mushroom-potato-and-wild-rice-soup-with-paprika-and-dill)
