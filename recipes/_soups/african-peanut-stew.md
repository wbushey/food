---
title: African Peanut Stew
ingredients:
  vegetable oil: 1 Tbsp
  garlic: 4 cloves
  fresh ginger: 1 inch
  sweet potato: 1 medium (1 lb.)
  onion: 1 medium
  cumin: 1 tsp
  crushed red pepper: 1/4 tsp
  tomato paste: 1 6oz. can
  chunky peanut butter: 1/2 cup
  vegetable broth: 6 cups
  collard greens: 1/2 bunch or 2-3 cups chopped
  cilantro: 1/4 bunch
tags:
  - vegetarian
  - gluten-free
time: 55 minutes
servings: 6
---

1. Peel and grate the ginger using a small holed cheese grater. Mince the garlic. Sauté the ginger and garlic in vegetable oil over medium heat for 1-2 minutes, or until the garlic becomes soft and fragrant.
2. Dice the onion, add it to the pot, and continue to sauté. Dice the sweet potato (1/2 inch cubes), add it to the pot, and continue to sauté a few minutes more, or until the onion is soft and the sweet potato takes on a darker, slightly translucent appearance. Season with cumin and red pepper flakes.
3. Add the tomato paste and peanut butter, and stir until everything is evenly mixed. Add the vegetable broth and stir to dissolve the thick tomato paste-peanut butter mixture. Place a lid on the pot and turn the heat up to high.
4. While the soup is coming up to a boil, prepare the collard greens. Rinse the greens well, then use a sharp knife to remove each stem (cut along the side of each stem). Stack the leaves, then cut them into thin strips. Add the collard strips to the soup pot.
5. Once the soup reaches a boil, turn the heat down to low and allow it to simmer without a lid for about 15 minutes, or until the sweet potatoes are very soft. Once soft, smash about half of the sweet potatoes with the back of a wooden spoon to help thicken the soup. Taste the soup and add salt if needed (will depend on the brand of broth used).
6. Serve the stew hot with a few cilantro leaves if desired.

Notes: Mustard greens or lacinato kale can be used in place of the collard greens.

[Source](http://www.budgetbytes.com/2014/08/african-peanut-stew-vegan/)
