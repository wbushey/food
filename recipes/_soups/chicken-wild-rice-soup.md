---
title: Chicken Wild Rice Soup
ingredients:
  olive oil: 2 tablespoons 
  onion: 1 large yellow
  carrots: 3 medium
  celery stalks: 3 large
  garlic: 2 medium cloves
  chicken broth: 12 cups (3 quarts)
  wild rice blend: 1 1/2 cups 
  boneless, skinless chicken breasts: 1 pound
  skinless chicken thighs: 1 pound boneless
  Italian parsley: 1/4 cup
tags:
  - poultry
  - gluten-free
time: 60 minutes
servings: 8-10
---

0. Prep: chop onion fine, chop celery, peel, chop carrots fine, mince garlic, coarsely chop parsley. Medium dice chicken.
1. Heat oil in a large pot over medium-high heat. When it shimmers, add onion, carrots, celery, and garlic. Cook, stirring occasionally, until onion is softened, about 10 minutes.
2. Add broth and rice, season with salt, and bring to a boil. Reduce heat to medium low and simmer, covered, until rice is tender but still has some firmness, about 25 to 30 minutes.
3. Add chicken, and season with freshly ground black pepper. Simmer until chicken is cooked through, about 10 minutes. Remove from heat, add parsley, taste, and season with additional salt and pepper as needed.

Notes: I have used just wild rice and usually just do 1 package of chicken breast from the co-op.

[Source](http://www.yummly.com/recipe/external/Chicken-and-Wild-Rice-Soup-Recipe-Chow-48036)
