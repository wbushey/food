---
title: Potsticker Soup
ingredients:
  olive oil: 1 Tbsp
  toasted sesame oil: 1.5 tbsp
  frozen potstickers or dumplings: 12 oz
  broth: 4 cups poultry or vegetable
  water: 1 cup
  shiitake mushrooms: 8 oz
  garlic: 3 cloves
  fresh ginger: 1 inch
  leek: small
  carrots: 1
  bok choy: 1 baby
  soy sauce: 1.5 tbsp
  salt: to taste
  pepper: to taste
  better than bouillon: optional, 2 tsp
  green onions: optional
  sesame seeds: optional, topping
  furikake seasoning: optional, topping
tags:
  - vegetarian
  - poultry
  - pork
time: 30 minutes
servings: 4
---

This does not store well, as the dumplings break down in the broth over time.

Can easily have meat or be vegetarian depending on potstickers/dumplings used.

1. Clean and chop leek. Clean and slice mushrooms. Mince garlic. Chop carrots. Clean and chop, bok choy seperate leaves from stems. Grate ginger
2. Heat olive oil in dutch oven/large pot. Saute leeks, carrots, garlic, ginger, mushrooms, and firm bok choy steams until leeks, mushrooms, and carrots soften; ~5 minutes. Lightly season with salt and pepper.
3. Add soy sauce, broth, water, and sesame oil. Bring soup to a light boil, stirring frequently. 
4. Carefully add frozen potstickers and boil as directed by package (usually ~3-5 minutes).
5. Reduce heat to low, stir in bok choy leaves and green onions if using
6. Serve, top with sesame seeds and/or green onions and/or furikake if using


[Source 1](https://soupaddict.com/potsticker-soup/) and [Source 2](https://howsweeteats.com/2021/01/potsticker-soup)
