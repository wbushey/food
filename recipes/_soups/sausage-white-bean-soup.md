---
title: Sausage White Bean Soup
ingredients:
  ground Italian sausage: 1 1/4 lb
  onion: 1 small
  garlic: 4 cloves
  Italian seasoning: 1 tsp
  thyme: 1 tbsp
  red pepper flakes: 1/4 tsp
  carrots: 2
  cannellini beans: 15 oz
  chicken stock: 4 cups
  fresh spinach: 6 oz
  heavy cream: 1/2 cup
  salt: to taste
  pepper: to taste
tags:
  - gluten-free
time: 40  minutes
servings: 6
---

1. Chop onion and carrots, mince garlic. Rinse and drain beans.
2. In a large saucepan over medium heat, add crumbled Italian sausage, onion, garlic, Italian season, thyme, and red pepper flakes. Cook and stir frequently for about 5 minutes, until the onion softens and starts to lightly brown.
3. Stir in carrots and half of the beans.
4. Pour in chicken stock, stir well, cover and bring to a boil.
5. Reduce to a simmer and cook for 15 - 20 minutes, until carrots are tender.
6. Add spinach and the remaining beans. Cover with lid and let the spinach wilt on low heat, stirring occasionally.
7. Once the spinach has wilted, remove from heat and stir in cream.
8. Adjust consistency with more stock or water to thin the soup, or more cream to thicken.
9. Season with salt, pepper, and additional red pepper flakes if desired.
10. Top with fresh thyme when serving.

[Source](https://juliasalbum.com/sausage-white-bean-soup/)
