---
title: Harira (Spiced Moroccan Vegetable Soup with Chickpeas, Cilantro, and Lemon)
ingredients:
  olive oil: 4 Tbsp
  onion: 1 large diced (2 cups)
  celery: 3 stalks diced
  carrots: 3 large peeled and cut
  ground turmeric: 1/2 tsp
  ground cumin: 1 tsp
  dried chile flakes: 1/2 to 1 tsp
  parsley: 1 bunch chopped and divided
  cilantro: 1 bunch chopped and divided
  tomatoes: 1 can diced
  chicken or veg stock: 7 cups
  green lentils: 1 cup
  black pepper: 1 tsp
  flour: 2 Tbsp
  egg: 1
  lemon juice: 1/8-1/4 cup (1 lemon)
  salt: 2 tsp
tags:
  - vegetarian
  - gluten-free
time: 1 hour
servings: 8-10
---

1. Heat oil in skillet over medium heat, add onion, celery, carrots until onion turns translucent (5-10 min)
2. Add turmeric, cumin, chile flakes, 1 tsp salt, half each of the parsley and cilantro, tomatoes, stock and bring to a boil. If using soaked uncooked chickpeas, drain and add. Simmer 25 min. uncovered.
3. Add lentils, if using canned chickpeas add those, 1 tsp salt, 1 tsp pepper and simmer until lentils are cooked, about 20 min. (sometimes more)
4. whisk flour, egg, lemon juice together. stir into the soup. Simmer 5 more min and add remaining parsley and cilantro, add more salt to taste and serve. 
   
Notes: I use GF flour mix, could also probably use corn or potato starch. For dried chile flakes I use crushed red pepper. The original recipe says to add flour mixture to 2 cups water but I like thicker soup. Original recipe calls for 1/4 cup lemon juice or 2 lemons, I think that is too much. Green lentils are good because they stay somewhat firm, but brown work ok too- I wouldn't use red. I like to use a quart of chicken stock, one small thing of chicken bone broth, and supplement with veg better than bullion.

[Source](https://www.epicurious.com/recipes/food/views/spiced-moroccan-vegetable-soup-with-chickpeas-cilantro-and-lemon-harira)
