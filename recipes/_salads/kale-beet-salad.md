---
title: Kale Beet Goat Cheese Salad with Maple Balsalmic Dressing
ingredients:
  beets: 5-6 small red or golden
  pecans: 1/2 cup
  maple syrup: 1/3 cup, divided
  cayenne: dash
  curly kale: 1 bunch
  balsalmic vinegar: 2 Tbsp
  lemon juice: 1 Tbsp fresh
  EVOO: 1/4 cup
  crumbled goat cheese: 1/2 cup
tags:
  - vegetarian
  - gluten-free
time: 1 hour + for beets, 20 min prep salad
servings: 8
---

1. Roast beets: preheat oven to 450F. You can peel and cut up beets, oil, and put in foil or dutch oven, or roast whole beets skin on (takes longer) until tender.
2. Toast pecans in small skillet over medium heat until fragrant- 5 min
3. Pour 1/4 cup maple syrup over pecans, add dash cayenne. Boil 1-2 min until liquid evaporates.
4. Pour pecans on parchment/wax paper, spread out to cool and dry
5. Clean kale and pat dry. Cut/tear into pieces, sprinkle with salt and massage 2-3 min until wilty.
6. In small bowl, whisk balsalmic vinegar, 1 Tbsp maple syrup, lemon juice. Drizzle olive oil slowly into mixture while whisking, emulsify. Season with salt and pepper.
7. Add roasted beets and goat cheese to kale. Break apart candied pecans and sprinkle onto salad. Serve with dressing.

   
Notes: I cut up the beets to roast because it's faster. We like to add a pan-cooked chicken breast and make this a meal. I use the whole small thing of goat cheese. Original recipe says you can use feta instead; I'm skeptical. Only combine with dressing to serve; to save, store salad and dressing separately.

[Source](https://toriavey.com/toris-kitchen/kale-and-roasted-beet-salad-with-maple-balsamic-dressing/)
