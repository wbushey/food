---
title: Coleslaw
ingredients:
  cabbage: 1 medium
  carrots: 3 medium peeled, shredded
  parsley: 1/2 cup fresh coarsley chopped
  mayo: 1 cup
  apple cider vinegar: 1-2 Tbsp
  Dijon mustard: 2 Tbsp
  celery seeds: 1 tsp
  salt: 1/4 tsp
  pepper: 1/4 tsp
tags:
  - vegetarian
  - gluten-free
time: 20 min
servings: 10
---

1. Cut and finely shred cabbage- remove core. Put in large bowl.
2. Add shredded carrots and chopped parsley to mix.
3. In separate bowl, stir mayo, vinegar, mustard, celery seeds, salt, pepper together and taste, adjust as desired.
4. Pour 2/3 of dressing over cabbage/carrot mix and stir; if it seems dry, add more dressing.
   
Notes: I try to get THE SMALLEST cabbage possible; half a small cabbage is even better. I like less vinegar and tend to start with 1 Tbsp and add after tasting. This recipe does not have added sugar like most coleslaws. I usually add all the dressing.

[Source](https://www.inspiredtaste.net/26522/coleslaw-recipe/#itr-recipe-26522)
