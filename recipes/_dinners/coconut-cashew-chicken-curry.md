---
title: Coconut Cashew Chicken Curry
ingredients:
  chicken: 2 lbs skinless boneless
  red pepper: 1
  red potatoes: 2
  onion: 1
  chicken broth: 2 cups
  coconut milk: 2 cups unsweetened
  cashews: 1 cup
  rice: 1 cup
  yellow curry powder: 3 tbsp
  cumin: 1 tsp
  salt: 1 tsp
  cayenne pepper: 1/2 tsp
  cooking oil:
  garlic powder: optional - 1 tbsp
  cilantro: optional
tags:
  - poultry
  - gluten-free
time: 3 1/2 hours - 5 1/2 hours (30 minutes prep, 3 - 5 hours cooking)
servings: 5
---

1. Make chicken broth if necessary.
2. Chop onion, saute in oil for 5 minutes. Add to slow cooker.
3. Thinly slice and seed pepper. Chop potatoes. Add to slow cooker.
4. Pound chicken. Chop into bite-size pieces. Add to slow cooker.
5. In a medium bowl whisk together chicken broth, curry powder, cumin, salt, and cayenne pepper ( and garlic powder). Pour mixture into slow cooker and stir to coat chicken and veggies.
6. Cover and cook on high for 3 hours, or low for 5 hours.
7. Chop cashews (and cilantro).
8. 40 minutes before slow cooking is done, begin cooking rice.
9. 15 minutes before slow cooking is done, stir in coconut milk.
10. When slow cooking is done, stir. Serve with rice and cashews( and cilantro).

[Source](https://www.lecremedelacrumb.com/slow-cooker-curry-cashew-chicken/)
