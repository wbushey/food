---
title: Rice Cooker Rice and Beans 
ingredients:
  rice: 1 cup
  salsa: 1/2 jar 
  chicken broth: 2 cups
  black beans: 1 can
  corn: 2 cups - optional
  cheese: optional
tags:
  - vegetarian
  - gluten-free
time: 35 minutes 
servings: 2
---

1. Put rice, drained black beans, salsa, corn, and broth in the rice cooker.
2. Stir
3. Cook in rice cooker.
4. Serve with optional cheese.
