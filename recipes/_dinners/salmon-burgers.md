---
title: Salmon Burgers
ingredients:
  salmon: 1+ lb
  shallot: 2 cloves
  mayonnaise: 1 tbsp
  dijon mustard: 2 tbsp
  lemon juice: 1 tbsp
  lemon zest: 1/2 tsp
  cayenne pepper: pinch
  salt: 1/2 tsp
  black pepper: to taste
  olive oil: 2 tbsp
  gluten-free breadcrumbs: 1 cup + 2 tbsp
  arugula: garnish, optional
  tartar sauce: garnish, optional
tags:
  - fish
  - gluten-free
time: 90 min
servings: 4
---

1. Chop shallot, place in bowl.
2. Remove skin from salmon. Cut 3/4 of salmon into 1/4-inch pieces and put in bowl.
3. Cut remaining salmon into chunks and put into food processor. Add mustard, mayonnaise, lemon juice, lemon zest, and cayenne. Pulse to make paste.
4. Add paste, 2 tbsp breadcrumbs, salt, and black pepper to bowl. Gently mix until just combined.
5. Divide mix into 4 mounds. Form burger patties and place on plate. Cover loosely with plastic wrap and refrigerate at least 30 minutes.
6. Preheat skillet and oil over medium-high heat.
7. Spread 1 cup breadcrumbs on plate. Press both sides of patties into breadcrumbs, then place in skillet.
8. Cook patties 3 to 4 minutes each side, then until internal temperature reaches 145F.


[Source](https://www.foodnetwork.com/recipes/food-network-kitchen/perfect-salmon-burgers-recipe-2105786)
