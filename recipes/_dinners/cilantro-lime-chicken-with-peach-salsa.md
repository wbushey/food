---
title: Cilantro-Lime Chicken
ingredients:
  chicken breasts: 1 lb
  cilantro: 1/4 bunch
  lime juice: 2 tbsp (~1 half fresh lime)
  honey: 2 tbsp
  green onions: 2
  chili powder: 1 tsp
  salt:
  black pepper: 
  butter: 2 tbsp
  olive oil: 2 tbsp
tags:
  - poultry
  - gluten-free
time: 20 minutes
servings: 2
---

Notes:
  * Pairs well with [Peach Salsa](/sides/peach-salsa.html)
  * Can be cooked on grill or in skillet on stove-top

## Prep

1. Chop cilantro and green onion. Squeeze lime juice.

## To Cook on Stove Top

2. Cut chicken into slices.
3. Pre-heat skillet over medium heat.
4. Season chicken with salt, pepper, and chili powder.
5. Cook chicken in olive oil, 4 minutes one side, then 2 minutes other side.
6. Add lime juice and honey; cook for additional 2 minutes, stirring frequently.
7. Remove skillet from heat; add butter and stir to coat chicken. Stir in chopped green onion.

## To Grill
2. Pre-heat grill to ~350F.
3. Season chicken with salt, pepper, and chili powder.
4. Grill chicken until cooked.
5. Preheat skillet over medium heat.
6. Slice cooked chicken.
7. Add butter, lime juice, and honey to skillet, stir quickly. Add sliced chicken and stir to coat. Stir in chopped green onion.


[Source](https://juliasalbum.com/chicken-with-peach-salsa/)
