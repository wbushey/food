---
title: London Broil
ingredients:
  black pepper: 1/2 tsp
  flank steak: 2 lbs.
  garlic: 1 clove
  ketchup: 1 tbsp
  olive oil: 1 tbsp
  oregano: 1/2 tsp
  salt: 1 tsp
  soy salt: 3 tbsp - gluten-free
tags:
  - beef
  - gluten-free
time: 30 minutes
servings: 4
---

1. In a bowl, mix together chopped garlic, salt, soy sauce, ketchup, oil,
   pepper, and oregano.
2. Aerate steak.
3. Place steak and marinade in bag. Let sit for 2 hours to overnight.
4. Grill steak.

[Source](http://allrecipes.com/recipe/14562/london-broil-ii/)
