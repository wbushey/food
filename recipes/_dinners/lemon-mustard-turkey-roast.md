---
title: Lemon Mustard Turkey Roast
ingredients:
  turkey: 2+ lbs roast or breast
  carrots: 3
  onions: 2
  potatoes: 2 lbs 
  garlic: 4 cloves
  dijon mustard: 1/4 cup
  whole grain mustard: 1/4 cup
  lemon: 1
  thyme: 1 tsp dried
  salt: 1 tsp
  black pepper: 1/2 tsp
  dry white wine: 1/2 cup
  vegetable broth: 1/4 cup
  olive oil: 2 tbsp
tags:
  - poultry
  - gluten-free
time: 4 hours
servings: 6
---

# Marinade

1. In a small bowl, combine both mustards, thyme, salt, pepper, oil, zest from lemon, and juice from lemon. Rub this mixture over the turkey (apply under the skin for best flavor) and marinate in refrigerator for at least 1 hour, at most one day.

# Roast

1. Preheat oven to 450F.
2. Rinse carrots and potatoes. Cut carrots, onions, and potatoes into large chunks and place on a roasting pans. Mince garlic and add to roasting pan. Set the turkey on top of the vegetables and bake uncovered at 450F for 20 minutes.
3. Pour the wine and stock over the vegetables. Cover the pan and lower the oven temperature to 350F. Bake until turkey reaches 165F in thickest part; approximately 2 hours.
4. Allow roast to rest for 20 minutes.

Source: Gluten-free Cooking with Tanya
