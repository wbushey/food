---
title: Salmon
ingredients:
  lemon: 1
  salmon: 1 fillet
  butter: 1/2 Tbsp
  salt and pepper: dash
tags:
  - fish
  - gluten-free
time: 30 minutes
servings: 2
---

# Baked

0. Preheat oven to 300
1. Slice the lemon, place on aluminum foil
2. Put fish on top of lemon slices, skin side down so it is touching the lemon
3. Put a couple pats of butter on top of the fish
4. Wrap tightly in foil and bake for 25 min
