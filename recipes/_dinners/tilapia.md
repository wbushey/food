---
title: Tilapia
ingredients:
  tilapia: 4 fillets
  lemons: 2
  butter: 3tbsp
  garlic powder: 1-1/2 tsp
  capers: 2 tbsp
  oregano: 1/2 tsp dried
  paprika: 1/8 tsp
  salt:
tags:
  - fish
  - gluten-free
time: 30 minutes
services: 3
---

# Baked

1. Preheat oven to 425
2. In a small bowl, combine melted butter, juice of one lemon, garlic powder, and salt.
3. Place tilapia in an ungreased baking dish. Cover tilapia with butter mix. Sprinkle
   with drained capers, oregano, and paprika.
4. Baked, uncovered, at 425 until fish is just beginning to flake; about 10 to 15 minutes.
5. Serve with wedges from second lemon.
