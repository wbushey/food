---
title: Maple Salmon
ingredients:
  salmon: 1 fillet (approximately 3/4 lb)
  garlic: 1 clove
  maple syrup: 1/4 cup
  soy sauce: 2 tbsp
  garlic salt: 1/4 tsp
  black pepper: 1/8 tsp
tags:
  - fish
  - gluten-free
time: 60 minutes
servings: 2
---

1. Mince garlic
2. Stir maple syrup, soy sauce, minced garlic, garlic salt, and pepper in a small bowl.
3. Cut salmon into 2 equal sized fillets. Place in a shallow glass baking dish and coat with maple syrup mix. Cover and marinate in fridge for 30 minutes, turning once halfway.
4. Preheat oven to 400 F
5. Place baking dish in oven, bake salmon uncovered until salmon reaches 145 F - approximately 20 minutes.

[Source](https://www.allrecipes.com/recipe/51283/maple-salmon/)