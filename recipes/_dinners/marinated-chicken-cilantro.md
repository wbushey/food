---
title: Marinated Chicken Cilantro 
ingredients:
  chicken breasts: 2
  cilantro: 3 tbsp chopped
  limes: 2
  olive oil: 2 tbsp
  sugar: 1/2 tsp
  salt: 1/2 tsp
  black pepper: 1/4 tsp
tags:
  - poultry
  - gluten-free
time: 30 minutes
servings: 2
---

1. Depending on size, halve or pound chicken breasts.
2. In a mixing bowl, mix zest and juice of limes, olive oil, chopped cilantro,
   sugar, salt, and black pepper.
3. Place chicken and marinade in bag, let sit for 30 minutes to overnight.
4. Grill chicken.

[Source](http://www.simplyrecipes.com/recipes/grilled_cilantro_lime_chicken/)
