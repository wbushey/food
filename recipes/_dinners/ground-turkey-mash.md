---
title: Ground Turkey Mash
ingredients:
  ground turkey: 1 lbs
  onion: 1/2
  green pepper: 1
  tomato: 1
  carrots: 2
  celery: 2
  garlic: 2 cloves
  rice: 1 cup
  ginger:
  thyme:
  cumin:
  oregano:
  black pepper: dash
  chicken boullon: 1 tbsp
  soy sauce - Gluten Free:
tags:
  - poultry
  - gluten-free
time: 30 - 40 minutes
servings: 2
---

1. Cook Rice
2. Chop/slice onion, garlic, and carrots. Saute these.
3. Chop and add green pepper, tomato, and celery to the saute. Season with
   ginger, thyme, and soy sauce.
4. Remove all to plate.
5. Brown turkey. Season with cumin, oregano, and black pepper. Drain.
6. Add veggies, 1 cup water, chicken boullon soy sauce, thyme, and ginger. 
   Simmer until reduced.
7. Serve over rice.
