---
title: Korean Short Ribs 
ingredients:
  beef ribs: 3lbs - boneless
  yellow onions: 1 cup
  garlic: 8 cloves
  fresh ginger: 1"
  asian pear: 1
  mirin: 1/4 cup
  soy sauce: 6 tbsp - gluten-free
  hot sauce: 1 tsp
  carrots: 4
  daikon: 1 medium
  salt: to taste
  scallions: 2 - optional
  toasted sesame seeds: to taste - optional
tags:
  - beef
  - gluten-free
time: 2 hours 
servings: 6
---

0. Cook rice if desired.
1. Remove fat from the ribs, and cut into 3" pieces.
2. Brown ribs for 2 minutes on each side.
3. Combine onion, pear, garlic, ginger, mirin, soy sauce, and hot sauce in a food 
   processor and blend until smooth. Add salt and/or hot cause to taste.
4. Arrange ribs on bottom of pressure cooker in a single layer. Pour blended souce
   over and stir to coat.
5. Cook ribs and sauce at high pressure for 35 minutes. Quick release pressure
   when done.
6. Peel and cut carrots and daikon into 1" peices. Slice scallions.
7. When pressure cooking is done, add carrots and daikon, and cook at high
   pressure for 3 additional minutes. Quick release pressure when done.
8. Serve with optional rice, scallions, and sesame seeds.

[Source](https://smittenkitchen.com/2018/02/korean-braised-short-ribs/)
