---
title: Pistachio Crusted Salmon
ingredients:
  salmon: 1 fillet (approximately 3/4 lb)
  garlic: 1 clove
  dijon mustard: 1/4 tsp
  lemon juice: 2 tbsp
  honey: 1 tbsp
  pistachios: 1/3 cup
  olive oil: 1 tbsp
  salt:
  pepper:
tags:
  - fish
  - gluten-free
time: 30 minutes
servings: 2
---

 1. Preheat oven to 375 F
 2. Coursely chop pistacios, mince garlic.
 3. Rinse salmon and pat dry, then place on parchment-lined baking sheet. Season with salt and pepper.
 4. In a small bowl, combine garlic, olive oil, mustard, lemon juice, and honey. Spread 3/4 of mixture on the salmon.
 5. Add pistachios to the remaining mix and stir to combine. Spoon the pistacio mix on top of the salmon and press lightly into salmon using the back of a spoon.
 6. Bake until the salmon reaches 145 F - approximately 15 - 20 minutes. Remove from oven and let rest for 5 minutes.

[Source](https://www.aheadofthyme.com/pistachio-crusted-salmon/)