---
title: Barbacoa
ingredients:
  beef roast: 2.5 lbs, chuck or round bottom
  onion: 1 medium
  garlic: 6 cloves
  chipotle peppers: 2 to 4
  lime juice: 1/4 cup (or ~1/2 lime)
  apple cider vinegar: 3 tbsp
  beef broth: 1 cup
  cumin: 4 tsp
  oregano: 1 tbsp
  cloves: 1/2 tsp ground
  tomato paste: 1 tbsp
  bay leaves: 2
  salt:
  pepper:
  oil: (avacado recommended)
tags:
  - beef
  - gluten-free
time: 3 - 6 hours
servings: 4
---

1. Cut roast into 4 large pieces. Season the meat with salt and pepper on all sides; let sit for 15 minutes.
2. Chop onion, garlic, and peppers.
3. Combine vinegar, lime juice, garlic, chipotle peppers, beef broth, cumin, oregano, cloves, tomato paste, and half the onions in a blender and blend until smooth.
4. Sear roast pieces.
5. Place roast pieces in pressure cooker with blended sauce and bay leaves. Pressure cook on high for 60 minutes.
6. Allow pressure to release naturally.
7. Remove roast pieces to a bowl. Shred pieces with two forks.
8. Toss beef in the juice; allow beef to soak up juice for 10 minutes.

Serve in tacos, sandwiches, nachos, burrito bowls.

Based on [this recipe](https://littlespicejar.com/pressure-cooker-barbacoa-beef/).
