---
title: Pork Roast
ingredients:
  pork tenderloin: 2 lbs
  oregano: 1 tsp
  onion salt: 1/2 tsp
  pepper: 1/2 tsp
  garlic salt: 1/4 tsp
  carrots: 3
  potatoes: 2 medium
  onions: 1 large
  garlic: 3 cloves
  chicken broth: 2 cups
  arrowroot powder: 1/3 cup
  water: 1/3 cup
  browning sauce: 1/4 tsp - optional
tags:
  - pork
  - gluten-free
time: 8 hours
servings: 6
---

1. Combine seasonings, rub over roast. Wrap roast in plastic wrap and refrigerate overnight.
2. Chop onions and garlic; saute.
3. Brown pork two minutes eat side over medium-high heat.
4. Chop potatoes and carrots, place in crock pot. Add onions, garlic. Place pork on top.
5. Deglaze pans used for onions, garlic, and pork with some broth. Add to crock pot.
6. Add rest of broth to crock pot.
7. Cook on low for 8 hours, or until pork is 140 degrees F.
8. Transfer roast and vegetables to a serving platter and keep warm.
9. Pour broth into a saucepan.
10. Combine arrowroot powder and water. Add to broth.
11. Bring broth to a broil; broil and whisk for 2 minutes.
12. Add browning sauce if desired.
13. Serve roast and veggies with sauce.
