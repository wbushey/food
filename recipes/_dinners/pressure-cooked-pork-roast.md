---
title: Pressure Cooked Pork Roast 
ingredients:
  pork sholder/butt: 2-3 lbs
  russet potatoes:  2 large
  mushrooms:  8 oz (sliced)
  carrots:  2
  garlic: 4 cloves
  unsalted butter: 1 tbsp
  chicken broth:  1 cup
  soy sauce - gluten free:  2 tbsp
  balsamic vinegar: dash
  bay leaves: 2
  arrowroot powder:  2 tbsp
  black pepper: to taste
  salt: to taste
  olive oil:
tags:
  - pork
  - gluten-free
time: 2 hours 
servings: 4
---

1. Make chicken broth, if needed. Dump into container.
2. Season pork with salt and pepper on each side.
3. Brown the pork in olive oil for 3 to 4 minutes on each side.
4. Mince garlic, chop carrots and potatoes into large chunks.
5. Saute mushrooms in butter, salt, and pepper for 7 to 12 minutes.
6. Cut pork into 0.5 inch slices.
7. Add garlic, carrots, and bay leaves to mushrooms. Saute for 3 more minutes.
8. Deglaze pot with mix of chicken broth, soy sauce, and balsamic vinegar.
9. Add pork to pot. Add potatoes on top.
10. Cook at high pressure for 5 minutes. Allow pressure to release naturally.
11. Move pork and vegetables to serving dish.
12. Mix arrowroot powder with 2 tbsp of water; mix this mixture into pot to make gravy.
