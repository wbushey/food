---
title: Pulled Chicken Cilantro Lime Tacos
ingredients:
  chicken breasts: 2 lbs
  taco seasoning: 1 packet
  salsa: 1 16 oz jar
  fresh cilantro: 1/3 cup
  lime juice: 1/4 cup (2 limes)
  tortillas: some
  taco toppings: some
tags:
  - poultry
  - gluten-free
time: 20 min
servings: 8
---

1. Place chicken breasts in pressure cooker. sprinkle taco seasoning over top, cover with salsa, lime juice, cilantro.
2. Place lid and pressure cook on high for 10 min. Let release naturally for ten min., release any remaining pressure and open.
3. Remove chicken breasts into bowl and shred with two forks. Add liquid from pot back in as desired.
4. Serve with warmed corn tortillas and taco toppings.
 
Notes: Original recipe says to add 1/2 cup water in instant pot, but I have never done this. Also says you can use frozen chicken, just set high pressure to 20 min plus 10 min. natural release. Can freeze in ziploc bags.

[Source](https://belleofthekitchen.com/slow-cooker-cilantro-lime-chicken-tacos/#recipe)