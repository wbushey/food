---
title: Reverse-Sear Steak
ingredients:
  beef steak:
  salt:
  high-heat oil:
tags:
  - beef
  - gluten-free
time: 8 - 24 hours
servings: 2
---

 1. Season steak on both sides with salt. Place on cooling rack on a baking sheet in refrigerator for 6 to 24 hours.
 2. Preheat oven to 200 degrees. Roast steak in oven until internal temperature of 120 degrees - 40 to 60 minutes depending on size and thickness of steak.
 3. Remove steak from oven, let rest for 10 minutes.
 4. Heat cast iron pan on high heat for 10 minutes.
 5. Brush a light coat of oil on both sides of steak.
 6. Sear steak on each side for 45 seconds.
 7. Let steak rest for 5 minutes.

[Source](https://www.cookingchanneltv.com/recipes/alton-brown/reverse-sear-ribeye-steak-reloaded-5458694)
