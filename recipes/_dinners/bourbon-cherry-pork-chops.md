---
title: Bourbon-Cherry Pork Chops
ingredients:
  pork chops: 4 8-10 oz
  cherries: 8 oz
  sweet potato: 2 lbs
  chicken broth: 1 cup
  bourbon: 1/4 cup
  orange marmalade: 1 tbsp
  butter: 7 tbsp
  pumpkin pie spice: 1/4 tsp
  cornstarch: 1 tsp
  salt: 2 tsp
  pepper: 1.75 tsp
tags:
  - pork
  - gluten-free
time: 1 hours
servings: 4
---

Note: Ground cinnamon can be substituted for pumpkin pie spice.

1. Make chicken broth if needed
2. Peel sweet potatoes and slice 1/4" thick
3. Combine sweet potatoes, 1/2 cup broth, 4 tbsps butter, 3/4 tsp salt, and 1/2 tsp pepper in large saucepan. Cover and cook over medium heat, stirring occasionally, until potatoes are tender; ~20 minutes. 
4. Pat pork chops dry and sprinkle with pumpkin pie spice, 1 tsp salt, and 1 tsp pepper. Melt 1 tbsp butter in skillet over medium-high heat. Cook pork chops until well browned and registers 140 degrees; ~ 6 minutes per side. Transfer pork to plat and tent with foil.
5. Process sauce with immersion blender until smooth.
6. Whisk cornstarch and remaining 1/2 cup broth together in bowl. Combine cherries, bourbon, orange marmalade, broth/cornstarch mixture, 1/4 tsp salt, and 1/4 tsp pepper in skillet. Bring to simmer over medium-high heat and cook, stirring until mixture turns think and translucent; ~3 minutes. Off heat, stir in remaining 2 tbsp butter.
7. Serve pork chops and sweet potatoes with sauce.

