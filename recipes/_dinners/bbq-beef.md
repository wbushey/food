---
title: BBQ Beef
ingredients:
  beef: 2 lbs, brisket or tri-tip
  liquid smoke:
  black pepper:
  garlic powder:
  onion powder:
  paprika:
  cumin:
  coriander:
  sugar:
  salt:
  bbq sauce:
tags:
  - beef
  - gluten-free
time: 10 hours
servings: 4
---

1. In a bowl, drench beef in liquid smoke.
2. Apply dry rub: add a lot of black pepper, garlic powder, and onion powder; then a bit of paprika; then a bit less cumin, coriander, and sugar; finally season with salt. Rub into meat, then repeat on other side. Season aggressively to account for liquid smoke flavor.
3. Cook in grill/oven at 225F, fat side up.
4. After 1 hour coat with a little additional liquid smoke. Add water for steaming.
5. After an additional 2 to 3 hours, insert thermometer into thickest part of beef, wrap beef tightly in aluminum foil, and return to grill/oven.
6. Cook at 225F, fat side up, until internal temp reaches 195F.
7. Let sit for 30 minutes.
8. Serve with BBQ sauce.

[Source](https://www.youtube.com/watch?v=AH8S4yuaMLY)
