---
title: Quick Pot Chicken
ingredients:
  sweet potatoes: 1 large
  Granny Smith apples: 2
  chicken thighs: 3, skin-on, bone-in
  chicken broth: 1 cup
  chopped pecans: 1/2 cup
  sage:
  lemon juice: dash
  salt and pepper: to taste
  chives: optional
tags:
  - poultry
  - gluten-free
time: 45 minutes
servings: 3
---

0. Make chicken broth if necessary
1. Cube sweet potatoe. Dice apples. Mince sage if fresh.
2. Prep cast iron skillet with oil and high heat. Season chicken with salt and pepper on skin side.
3. Once skillet is ripping hot, add chicken thighs, skin side down. Season meat side with salt and pepper. Flip thighs once skin side has browned and pulls away cleanly. Cook meat side for a few minutes for color. Don't cook the chicken all the way through.
4. Remove chicken and most chicken fat. Add sweet potates and apples. Sauté over medium heat for 5 to 7 minutes. Season with salt and pepper.
5. Pre-heat oven to 425F.
6. Deglaze with a few glugs of whiskey. After alcohol has burned off, add chicken broth and sage. Let broth reduce by half.
7. Add the chicken back to the pan, skin side up. Place pan in oven and cook until thickest part of chicken reaches 175F; approximately 25 minutes.
8. Toast pecans in another skillet.
9. Once chicken is cooked, add lemon juice.
10. Serve topped with toasted pecans and optional chives.

Based on this [One Pot Chicken recipe](https://basicswithbabish.co/basicsepisodes/weeknightmeals).
