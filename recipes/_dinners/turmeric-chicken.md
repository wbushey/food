---
title: Turmeric Chicken
ingredients:
  olive oil: 2 Tbsp
  red onion: 1/2
  ginger: 2 inches
  garlic: 1 Tbsp minced
  turmeric: 1/2 Tbsp
  ground cumin: 1/2 tsp
  cinnamon: 1/2 tsp
  crushed red pepper: 1/4 tsp
  bay leaf: 1
  chicken breasts/thighs: 2 lb
  diced tomatoes: 1 15 oz can
  coconut milk: 1 13.5 oz can
  cilantro: 1/4 bunch
  salt and pepper: some
  rice: 2 cups cooked jasmine or basmati
tags:
  - poultry
  - gluten-free
time: 55 min
servings: 6
---

1. Dice onion, mince garlic, grate ginnger, saute in large pot over medium heat with olive oil until onions soften
2. Add turmeric, cumin, cinnamon, red pepper. Saute 2 min. Dice chicken, add to pot. Saute until cooked through- 7-10 min.
3. Add can of tomatoes, bay leaf. Place lid on top and simmer 30 min. Turn off heat, add coconut milk, season with salt, pepper.
4. Cook rice
5. Serve with rice, top with cut up cilantro
   
Notes: if you cook this in a cast iron skillet, the turmeric will stick in the seasoning for several uses. Enameled dutch oven works great. Could add cashews to serve. I often forget the red onion and don't miss it.

[Source](https://www.budgetbytes.com/turmeric-chicken/)