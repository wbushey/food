---
title: Wine Braised Chicken
ingredients:
  chicken: 3 large boneless/skinless pieces
  carrots: 3
  onion: 1
  garlic: 3 cloves
  red wine: 1 cup dry (Pinot Noir or French Burgundy suggested)
  chicken broth: 2 cups
  tomato paste: 2 tbsp
  bay leaf: 1
  thyme: optional - 3 springs
  sage: optional - 12 leaves
  parsley: optional - 5 sprigs
  salt: to taste
  pepper: to taste
  olive oil:
tags:
  - poultry
  - gluten-free
time: 1.5 hours
servings: 6
---

1. Cut chicken pieces in half to create thin fillets. Pat fillets dry with paper towel.
2. In large, deep pot, heat olive oil over medium-high heat.
3. Season chicken with salt and pepper.
4. In batches, sear each chicken piece until browned on both sides; about 2 minutes per side. Place on side plate.
5. Add more olive oil if needed, then saute onion, garlic, and garlic. Add salt and pepper. Saute until onions are cooked, about 5 to 7 minutes.
6. Deglaze pot with wine. Scrape up fond.
7. Let wine simmer for a minute or two.
8. If using thyme/sage/parsley, prepare an herb bundle (including the bay leaf) using cheese cloth or food twine.
9. Add chicken broth and tomato paste; stir well. Add chicken back into the pot. Add herb bundle or bay leaf. Bring to a boil.
10. Once boiling, reduce heat to a simmer, and leave mostly covered for an hour. Stir periodically. If it starts to dry out, add a little water.

Based on recipes from https://www.thelifejolie.com/wine-braised-chicken/ add https://www.marthastewart.com/312948/wine-braised-chicken .
