---
title: Orange Marinaded Pork Chops 
ingredients:
  orange juice: 1/2 cup
  garlic: 2 cloves
  olive oil: 2 tbsp
  cumin: 2 tsp
  black pepper: to taste
tags:
  - pork
  - gluten-free
time: 30 minutes
servings: 4
---
1. Crush garlic. Make orange juice if neccessary.
2. Combine orange juice, garlic, olive oil, cumin, and black pepper to create
   marinade. 
3. Marinade pork chops for at least 2 hours.
4. Cook pork chops.
