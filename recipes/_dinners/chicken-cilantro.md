---
title: Chicken Cilantro
ingredients:
  onion: 1/2 small
  garlic: 2 cloves
  butter: 1 tbsp
  vegetable oil: 1 tbsp
  chicken breasts: 2
  salt: dash
  black pepper: dash
  cilantro: 12 stems
  lime juice: splash
tags:
  - poultry
  - gluten-free
time: 30 minutes
servings: 2
---

1. Chop onion, garlic, and cilantro.
2. Saute onions and garlic in oil and butter.
3. Add chicken, salt, pepper. Cook chicken 5 minutes each side.
4. Add cilantro, reduce heat, cook another 5 minutes.
