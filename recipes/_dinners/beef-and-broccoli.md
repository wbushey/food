---
title: Beef and Broccoli
ingredients:
  beef: 1.5 lbs stew or flank steak
  broccoli: 1 to 2 heads
  rice: 1 to 2 cups
  onion: 1
  garlic: 4 cloves
  beef broth: 8 oz
  soy sauce - gluten free: 1/2 cup
  brown sugar: 1/3 cup
  sesame oil: 2 tbsp
  cooking oil: olive or whatever
  red pepper flakes: 1/8 tsp
  cornstartch: 3 tbsp
  sesame seeds: optional
  black pepper: 
tags:
  - beef
  - gluten-free
time: 2 hours
servings: 4
---

1. If needed, cut the beef into strips about 1 inch long.
2. Season beef with black pepper.
3. [Cook the rice](/sides/stove-top-rice.html)
4. Chop onion, mince garlic, chop broccoli.
5. Brown the beef in a pot with cooking oil.
6. Saute onion and garlic in another pot.
7. Combine beef broth, soy sauce, brown sugar, sesame oil, and red pepper flakes in a bowl. Stir until sugar disolves.
8. Combine beef, onion, garlic, and sauce into pressure cooker. Cook on high pressure for 12 minutes.
9. While the beef is cooking, steam the broccoli for 8 to 10 minutes.
10. When pressure cooker is finished, create a slurry of cornstartch and water and add to pressure cooker. Simmer/Saute to a boil until the sauce thickens.
11. Serve rice, beef, and broccoli in a bowl. Garnish with optional sesame seeds.
