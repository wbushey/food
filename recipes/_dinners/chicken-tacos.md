---
title: Chicken Tacos
ingredients:
  oil: 1 tablespoon
  chicken breasts: 1 pound boneless skinless, cut into 1/2-inch strips
  McCormick® Chicken Taco Seasoning Mix: 1 package
  water: 1/2 cup
  taco shells: 12
tags:
  - poultry
  - gluten-free
time: 20 minutes
servings: 6
---

1. Heat oil in large skillet on medium heat. Add chicken; cook and stir 3 to 5 minutes or until no longer pink.
2. Stir in in Seasoning Mix and water.
3. Bring to boil. Reduce heat to low; simmer 5 to 7 minutes or until most of liquid is absorbed and chicken is cooked through, stirring occasionally. Spoon into warm taco shells. Serve with desired toppings.

Notes: My desired toppings include: fresh tomatoes, cilantro, bell pepper, lettuce, green onion, salsa, refried beans, cheese, guacamole

[Source](http://www.mccormick.com/Recipes/Main-Dishes/Chicken-Tacos)