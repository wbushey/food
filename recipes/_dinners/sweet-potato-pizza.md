---
title: Sweet Potato Crust Pizza
ingredients:
  sweet potato: 1 medium to large
  rice flour: 1.5 cups (approximate)
  baking powder: 1 tsp
  eggs: 2-3
  salt and pepper: dash
  pizza sauce: 1 can
  pizza toppings: as desired
tags:
  - vegetarian
  - gluten-free
time: 60 minutes
servings: 6
---

0. Preheat oven to 350
1. Cook sweet potato until soft- ~10 min on high in microwave, or roast in oven
2. When cool enough, skin and mash sweet potato
3. Slowly add rice flour and baking powder until dough consistency
4. Beat eggs into dough; it will form a kind of thick paste
5. Use spatula to spread dough-paste evenly on parchment paper lined baking sheet
6. Spread sauce on top, place toppings, and put in oven for ~20-40 min

Notes: Different types of sweet potatoes spread differently; I've had the best luck with the bright orange ones.
