---
title: Lechón Asado (Cuban Mojo Marinated Pork)
ingredients:
  pork shoulder: 3+ lbs
  olive oil: 3/4 cup
  orange zest: 1 tbsp
  orange juice: 3/4 cup
  lime juice: 1/2 cup
  cilantro: 1 cup
  mint: 1/4 cup
  garlic: 8 cloves
  oregano: 1 tbsp
  cumin: 2 tsp
  salt: to taste
  pepper: to taste
tags:
  - pork
  - gluten-free
time: 3 hours
servings: 6
---

# Prep and Marinade

1. Finely chop cilantro and mint. Mince garlic. Mince oregano if fresh.
2. In a large ziplock bag, combine olive oil, orange zest, orange juice, lime juice, cilantro, mint, garlic, oregano, and cumin. Shake to mix, then add pork shoulder.
3. Place marinating pork in fridge overnight.

# Cooking

1. Preheat oven to 425 F.
2. Place pork on a rack in a baking sheet. Salt and pepper the pork well.
3. Roast the pork for 30 minutes.
4. Reduce oven to 375 F. Roast for another 80 minutes, until internal temp is 160 F.
5. Let rest, covered with aluminum foil, for 20 minutes.
6. Carve and serve.

Based on [this recipe](https://thefoodcharlatan.com/cuban-mojo-marinated-pork-recipe/).
