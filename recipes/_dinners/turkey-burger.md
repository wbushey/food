---
title: Turkey Burgers/Meatballs
ingredients:
  ground turkey: 1 lb
  GF Panko breadcrumbs: 1/2 cup
  grated parmesean cheese: 1/2 cup
  egg: 1
  olive oil: 1 1/2 Tbsp
  onion powder: 1/2 tsp
  garlic powder: 1/2 tsp
  salt: 1/2 tsp
  pepper: 1/4 tsp
tags:
  - poultry
  - gluten-free
time: 45 min
servings: 4
---

1. Preheat oven to 400F, put parchment paper on baking sheet
2. Combine ingredients in large bowl, mix with hands
3. Scoop into preferred shape: small meatballs or burgers
4. Bake 25-30 min until golden and cooked through
5. To freeze: cool completely, place in ziploc. To reheat: defrost lightly in microwave and add to sauce or reheat fully in microwave.
   
Note: delicious with fresh/dried herbs (basil, oregano, thyme, chives) or nuts (walnuts). Works well to grill instead of bake. Could make into jucy lucy. 

[Source](https://meaningfuleats.com/turkey-meatballs-with-loaded-veggie-sauce-gluten-free/#recipe-card)
