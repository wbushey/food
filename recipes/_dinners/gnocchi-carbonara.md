---
title: Gnocchi - Carbonara
ingredients:
  gnocchi: 1 lb, gluten free
  mushrooms: 8 oz, sliced
  eggs: 3
  parmesan cheese: 1 cup
  peas: 1 cup
  black pepper: 1/2 teaspoon
  parsley: 1 tablespoon
  olive oil: 2 tablespoons
tags:
  - vegetarian
  - gluten-free
time: 30 minutes
servings: 4
---

Alternative: Replace mushrooms with 6 strips of bacon, cut into 1" pieces. Replace olive oil in step 8 with bacon grease.

1. Heat olive oil in a pan over medium heat.
2. In heated pan, cook mushrooms until they have released their moisture, ~ 10 minutes.
3. Chop parsley.
4. Bring a pot of salted water to a boil for the gnocchi.
5. In medium bowl, beat eggs with half the Parmesan and most of the parsley. Season with pepper. Set aside.
6. Add gnocchi to boiling water, bring back to boil. Sook until gnocchi are floating on top of the water, ~3 minutes.
7. If using frozen peas, defrost them.
8. Heat olive oil in a large pot.
9. Reserve 1 cup of gnocchi water, drain the rest.
10. Add cooked gnocchi to large pot and toss/mix to coat in oil.
11. Add egg mixture, toss gently to coat the pasta. And mushrooms and peas; toss to coat.
12. Stir everything and let cook for 2 - 3 minutes.
13. If the sauce is too think, add the reserved gnocchi water 1/4 cup at a time until the sauce is smooth and creamy.
14. Serve with remaining cheese and parsley.

[Source](https://semihomemaderecipes.com/gnocchi-carbonara/)
