---
title: Chicken Cacciatore
ingredients:
  chicken: 2 lbs skin-on/bone-in thighs and breasts
  mushrooms: 8 oz
  bell peppers: 2 red and/or yellow
  onion: 1
  garlic: 4 cloves
  diced tomatoes: 28 oz can
  red wine: 1/2 cup
  chicken broth: 1 cup
  capers: 2 tbsp
  oregano: 2 tsp dry, or 6 tsp fresh
  rosemary: 2 tsp dry, or 6 tsp fresh
  bay leaf: 1
  olive oil: 4 tbsp
  salt:
  pepper:
  parsley: optional
  parmesan: optional
  pasta: optional
  rice: optional
tags:
  - poultry
  - gluten-free
time: 90 - 120 minutes
servings: 5
---

1. Make Chicken broth if needed.
2. Season chicken pieces with salt and pepper.
3. Heat dutch oven over medium heat, add olive oil.
4. Brown chicken pieces about 3 minutes each side, working in batches. Set aside browned chicken.
5. Dice onion, bell peppers, and garlic.
6. Saute onion and bell peppers for 5 minutes. Add mushrooms and saute for another 3 minutes.
7. Push all vegetables to one side of pan. Add garlic, capers, oregano, and rosemary.
   Stir constantly for 1 minutes.
8. Stir in wine, cook for 3 minutes while scraping up brown bits.
9. Add diced tomatoes with liquid, chicken broth, and bay leaf. Bring to a low boil.
   Salt and pepper to taste.
10. Tuck browned chicken into sauce, bring to a simmer, reduce heat to low.
    Cover and cook for a minimum of 45 minutes, up to 2 hours.
11. Optionally cook pasta or rice to serve cacciatore on.
12. Optionally garnish with chopped parsley and/or grated parmesan.

[Based on this recipe](https://shewearsmanyhats.com/chicken-cacciatore-recipe/)
