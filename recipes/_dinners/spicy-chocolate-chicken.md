---
title: Spicy Chocolate Chicken
ingredients:
  chicken thighs: 1.5 lbs
  whole-fat chocolate milk: 1 cup
  jalapeños: 2
  chili powder: 2 tbsp
  salt: 2 tsp
tags:
  - poultry
  - gluten-free
time: 1 hour
servings: 4
---

 1. Split jalapeños length wise and scope out seeds.
 2. In saucepan, stir together chocolate milk, chili powder, jalapeños, and salt.
 3. Add chicken thighs. Bring mixture to a boil, reduce heat to low, cover, and simmer for 35 to 40 minutes.

This chicken pairs well with [Sautéed Zucchini](/sides/sauteed-zucchini) and rice.

[Source](https://www.washingtonpost.com/news/voraciously/wp/2020/05/12/chocolate-milk-simmered-chicken-dont-knock-it-until-you-try-it/)
