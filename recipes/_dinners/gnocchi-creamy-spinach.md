---
title: Gnocchi - Creamy Spinach
ingredients:
  gnocchi: 1 lb
  evaporated milk: 12 oz
  spinach: 12 to 16 oz frozen, chopped
  onion: 1
  garlic: 2 cloves
  cayenne pepper: 1/8 tsp
  nutmeg: 1/4 teaspoon freshly grated
  black pepper: 1/4 teaspoon
  salt: 1/2 teaspoon
  olive oil: 2 tbsp
tags:
  - vegetarian
  - gluten-free
time: 30 minutes
servings: 4
---

1. Thaw spinach (~10 minutes in microwave)
2. Squeeze excess water out of spinach
3. Chop garlic and onion, saute in oil for 5 minutes with salt, black pepper, nutmeg, and cayenne pepper.
4. Add spinach and evaporated milk and bring to a simmer.
5. Add gnocchi. Cover and cook, stirring occasionally, until gnocchi are soft and tender but not mushy, ~5 to 10 minutes.


[Source](https://www.washingtonpost.com/food/2022/12/09/gnocchi-creamy-spinach-recipe/)
