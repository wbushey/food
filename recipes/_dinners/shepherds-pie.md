---
title: Shepherd's Pie 
ingredients:
  ground turkey: 1.5 lb
  red potatoes: 2 lbs
  milk: 1/2 cup
  onion: 1 large
  garlic: 2 cloves
  carrots: 1 cup chopped
  celery: 1 cup chopped
  corn: 1 cup kernels
  peas: 1 cup
  beef broth: 1 cup
  cumin:  1/2 tsp
  oregano: 2 tsp
  parsley: 2 tsp
  rosemary: 2 tsp
  salt: to taste
  pepper: to taste
  baking powder: 1 tsp
  butter: 2 tbsp
  olive oil: 1 tbsp
tags:
  - poultry
  - gluten-free
time: 2 hours 
servings: 6
---

1. Make beef broth if needed.
2. Clean and chop the potatoes into large cubes. Place potatoes in pot, cover
   with water, bring to a boil, and let simmer for 15 minutes.
3. Rinse any frozen vegetables. Chop all vegetables. 
4. Saute vegetables in mix of oil and butter. Begin with carrots, add garlic
   and onions a few minutes later, add remaining vegetables another few minutes
   later.
5. Season the turkey with all spices, then brown it in cast iron skillet.
6. Preheat oven to 400F.
7. Drain the cooked potatoes, allow to dry for several minutes in a large mixing bowl.
8. When vegetables are tender, add them to the meat. Also add broth, and all
   spices except cumin. Let simmer for 10 minutes. 
9. Heat milk and 2 tbsp of butter until warm and mixed. Add baking powder.
10. Add milk mixture to potatoes, and mash.
11. When meat/vegetable mix is done simmering, spread mashed potatoes evenly on top.
    Use a fork to create lines and peaks in the potatoes.
12. Bake the pie for 30 minutes.
