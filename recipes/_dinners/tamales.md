---
title: Tamales 
ingredients:
  tamales: 4
tags:
  - poultry
  - pork
  - vegetarian
  - gluten-free
time: 30 minutes 
servings: 1
---

1. Boil water in a pot with steamer basket
2. Once water is boiling, place tamales in basket and cover. Reduce to a simmer and steam for 30 minutes.
