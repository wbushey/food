---
title: Turkey Burgers
ingredients:
  ground turkey: 1 lbs
  egg: 1
  onions: 1 small
  garlic: 1 clove
  oat flour: 1/4 cup
  salt: to taste
  pepper: to taste
  basil: 2 sprigs, optional
  parsley: to taste, optional
tags:
  - poultry
  - gluten-free
time: 1 hours
servings: 5
---

1. Make oat flour if neccessary.
2. Grate onion, mince garlic.
3. Mix together all ingredients.
4. Make five patties.
5. Grease grill surface
6. Grill high temperature 3 minutes each side, then medium-low for 5 minutes each side.
