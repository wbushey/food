---
title: Marry Me Chickpeas
ingredients:
  chickpeas: 28 oz
  vegetable stock: 1 cup
  coconut milk: 1 cup
  garlic: 2 cloves
  sun-dried tomatoes: 1/2 cup
  tomato paste: 2 tbsp
  olive oil: 2 tbsp
  basil: 6-8 fresh leaves
  Italian seasoning: 1/2 tbsp
  red chili flakes: 1/2 tsp
  thyme: 1 tsp fresh
  black pepper: 1/4 tsp
  salt: 
tags:
  - vegetarian
  - gluten-free
time: 30 minutes
servings: 4
---

1. Make vegetable stock if needed
2. Crush garlic, slice tomatoes, shred basil
3. Drain and rinse chickpeas
4. Saute garlic in large skillet with olive oil over medium heat until fragrant, about 1 - 2 minutes
5. Stir in Italian seasoning, red chili flakes, pinch of pepper. Cook for another minute.
6. Add tomato paste, stir to combine well with spices and garlic
7. Stir in tomatoes, thyme, chickpeas, and vegetable stock. Bring to a simmer, then cover with a lid and simmer for 5 minutes.
8. Stir in coconut milk, simmer for another minute, then add shredded basil.
9. Season to taste with salt and pepper.


[Source](https://vegancocotte.com/marry-me-chickpeas/)
