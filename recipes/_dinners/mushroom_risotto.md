---
title: Mushroom Risotto
ingredients:
  chicken broth: 4 cups
  white mushrooms: 1 cup
  brown mushrooms: 1 cup
  shallot: 1
  chives: 1 tbsp
  arborio rice: 2 cups
  dry white wine: 1 cup - Sauvignon Blanc or Chardonnay
  parmesan cheese: 1/3 cup
  butter: 4 tbsp
  olive oil: 3 tbsp
  rosemary: to taste
  salt: to taste
  black pepper: to taste
tags:
  - vegetarian
  - gluten-free
time: 60 minutes
servings: 4
---

1. Chop shallot and chives. Shred parmesan. Slice/chop mushrooms if needed.
2. Warm broth over low heat
3. Cook mushrooms in 2 tbsp olive oil over medium heat until soft, about 6 minutes. Season with rosemary, salt, and pepper.
4. Pour 1/2 cup wine into mushrooms. Increase heat to medium-high and simmer 3 to 5 minutes until wine reduces. Set aside mushrooms and their liquid.
5. Cook shallot in 2 tbsp butter and 1 tbsp olive oil over medium-low heat for 3 minute.
6. Add rice, stirring to coat with butter and oil, until rice has a pale, golden color - about 3 minutes.

## Stove Top Method

6. Pour 1/2 cup wine into rice, stirring constantly until wine is fully absorbed - about 3 minutes.
7. Add 1/2 cup broth at a time to rice, stirring continuously until the liquid is absorbed. Cook rice until it is al dente, about 20 minutes.

## Pressure Cooker Method

6. Combine 1/2 cup wine, chicken broth, rice, and shallot in pressure cooker
7. Cook on Risotto setting for 6 minutes. Release pressure when done.

## Finish

8. Remove rice from heat, stir in mushrooms with their liquid, 2 tbsp butter, chives, and parmesan. Season with salt and pepper.
