---
title: Cilantro Lime Honey Garlic Salmon
ingredients:
  salmon: 1 lb
  garlic: 3 clove
  honey: 2 tbsp
  lime juice: 1 tbsp (~1 quarter fresh lime)
  cilantro: 2 tbsp
  salt:
  black pepper: 
  olive oil: 1 tbsp
tags:
  - fish
  - gluten-free
time: 60 minutes
servings: 2
---

1. Preheat oven to 400F.
2. Mince garlic, chop cilantro, squeeze lime juice.
3. Pat salmon dry. Brush on olive oil. Place salmon on aluminum foil in baking sheet, fold sides of foil up tightly around salmon.
4. In a small bowl, combine honey, lime juice, cilantro, and garlic. Mix to combine.
5. Spread garlic honey sauce on salmon.
6. Bake salmon, uncovered, for 15 - 20 minutes (until salmon reaches 145F).
7. Remove salmon from foil and serve with garlic honey sauce.

[Source](https://juliasalbum.com/cilantro-lime-honey-garlic-salmon/)
