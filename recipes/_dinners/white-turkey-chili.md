---
title: White Turkey Chili 
ingredients:
  ground turkey: 1 1/2 lbs
  Monterey Jack cheese: 2 cups
  cannellini beans: 45 oz
  green chile peppers: 8 oz
  onion: 1
  garlic: 3 cloves
  chicken broth: 5 cups
  cumin: 1 tbsp
  oregano: 1 tbsp
  cinnamon: 1 tsp
  cayenne pepper: to taste
  black pepper: to taste
tags:
  - poultry
  - gluten-free
time: 45 minutes
servings: 4
---
1. Make chicken broth if needed.
2. Chop onion and garlic.
3. In large pot, cook onion, garlic, and seasoned turkey until turkey is brown;
   about 10 minutes.
4. Puree one can of cannellini beans.
5. Add chile peppers, cumin, oregano, cinnamon, cayenne pepper and black
   pepper. Saute for 5 minutes.
6. Add whole and pureed beans, chicken broth, and shredded cheese. Stir well
   and simmer for 10 minutes.
