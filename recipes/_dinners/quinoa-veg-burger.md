---
title: Quinoa Veggie Burgers
ingredients: 
  cooked quinoa: 2 cups
  eggs: 2
  oats: 1/2 cup
  coconut aminos: 1.5 Tablespoon
  chopped nuts: 1/2 cup
  herbs: 3 Teaspoons
  salt and pepper: to taste
tags:
  - vegetarian
  - gluten-free
time: 35 minutes
servings: 12
---

1. Preheat oven to 350F.
2. Mix cooked quinoa (I had cooked mine with onions, garlic, carrots, ginger, and better than bullion) with eggs, nuts (I used walnuts), herbs (I used Marjoram and Oregano about 1.5 tsp each- you might want more, mine were kind of bland), any veggies you want (I used leftover steamed broccoli and cooked sweet potato skins), and coconut aminos.
3. Add oats until you get a consistency that can be made into sloppy patties with your hands. My quinoa was pretty gloopy and with the eggs and coconut aminos it got wetter so I may have needed more than 1/3 cup oats.
4. Make 12 patties and place on parchment paper covering a baking sheet
5. Bake for 30-35 min; you don't need to flip them. They will become crispy.
6. The recipe says you can store them in the freezer.

[Loosely based on this recipe](http://www.eatingwell.com/recipe/250670/quinoa-veggie-burger/)