---
title: Chicken Poached on Stovetop
ingredients:
  chicken breasts: 1-4
  salt: 1/2 tsp
  pepper: 1/4 tsp
tags:
  - poultry
  - gluten-free
time: 20 min
servings: 1-4 chicken breasts
---

1. Place chicken breasts in bottom of large pot, season with salt and pepper.
2. Cover chicken with 1 inch water/stock
3. Place pot on stove, bring water to boil. Reduce heat and simmer until internal chicken temp is 165F; 8-16 min.
4. Dice, slice, or shred as needed

Notes: If using bone-in breasts increase cook time. If using thighs, reduce cooking time. I cover my pot and let it sit, then check at 8 min.

[Source](https://www.wellplated.com/how-to-cook-shredded-chicken/#wprm-recipe-container-33505)