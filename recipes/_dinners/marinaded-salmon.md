---
title: Marinaded Salmon
ingredients:
  salmon: ~1/2 lb fillets
  soy sauce: 1/4 cup
  honey: 1/8 honey
  citrus juice: 1/8 cup (orange or lemon)
  garlic: 2 cloves or garlic powder
  ground ginger: 1/2 tsp
  red pepper flakes: 1/8 tsp
tags:
  - fish
  - gluten-free
time: 1 hour
servings: 2
---

1. Combine all ingredients besides salmon fillets in a bowl or bag and mix.
2. Pat salmon fillets dry.
3. Marinade fillets for 30 minutes unrefrigerated, or 1 hour refrigerated.
4. Cook salmon.
