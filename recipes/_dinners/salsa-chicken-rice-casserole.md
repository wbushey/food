---
title: Salsa Chicken Rice Casserole 
ingredients:
  white rice: 1 1/3 cups
  chicken breasts : 2
  onion: 1 small
  cream of chicken soup - isGlutenFree: 1 can
  cream of mushroom soup - isGlutenFree: 1 can
  salsa: 1 1/2 cups
  monterey jack: 2 cups shredded - optional
  cheddar: 2 cups shredded - optional
tags:
  - poultry
  - gluten-free
dairy-free: true
time: 70 minutes
servings: 4
---

1. Cook rice.
2. Chop onion.
3. Saute chopped onion.
4. Pre-heat oven to 350F.
5. Cut chicken into bite-sized pieces. Lightly pepper chicken.
6. In a medium bowl, combine cream of mushroom soup, cream of chicken soup,
   onion, and salsa. Optional, mix in cheese.
7. Lightly grease 9x13 inch backing dish. Combine and mix rice, chicken, and 
   salsa mix.
8. Bake in preheated oven for about 30 minutes.
9. Optional, mix in cheese when serving.
