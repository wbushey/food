---
title: Grilled Chicken Thighs
ingredients:
  chicken thighs: 1 lbs
  salt: to taste
  pepper: to taste
  garlic powder: to taste
tags:
  - poultry
  - gluten-free
time: 40 minutes
services: 4
---
1. Trim excess fat from thighs
2. Season thighs with salt, pepper, and garlic powder.
3. Grill over high heat for 2 minutes each side, then medium for 10 minutes each side. Cook to ~ 175F.
