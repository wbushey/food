---
title: Moosewood Cajun Skillet Beans
ingredients:
  onion: 1 medium, chopped 
  garlic: 2 cloves, minced or presses 
  vegetable oil: 2 tablespoons
  celery: 1 cup or 3 stalks
  green or red bell peppers: 2 whole or 1 1/2 cups
  fresh thyme: 1 tsp chopped or 1/2 tsp dried
  fresh basil: 1 Tbsp chopped or 1 tsp dried
  fresh oregano: 1 tsp fresh or 1/2 tsp dried
  black pepper : 1/4 teaspoon ground
  cayenne: 1 pinch
  salt: 1 pinch 
  tomatoes: 2 cups chopped fresh or canned (14.5 ounce can) 
  honey: 1 tablespoon, or molasses 
  Dijon mustard : 1 tablespoon
  black-eyed peas: 4 cups cooked, or butter beans (2 10 ounce frozen packages or two 16-ounce cans, drained) 
  scallions: optional chopped garnish
  cheddar cheese: optional grated
tags:
  - vegetarian
  - gluten-free
time: 20 minutes
servings: 6
---

1. In a heavy saucepan or skillet, saute the onions and garlic in the oil on medium heat. Chop the celery and bell peppers and add them to the pan. Continue to saute for about five minutes, stirring occasionally. Add the thyme, basil, oregano, black pepper, cayenne, and salt. Cover and cook for five minutes or until the onions are golden, stirring once or twice. 
2. Add the tomatoes, honey or molasses, and mustard, and simmer for 5 more minutes. Add the beans, cover, and stir occasionally until thoroughly heated. Canned beans will be hot in less than 10 minutes, but frozen beans need to simmer for 15 to 20 minutes. 
3. Top with scallions or grated cheese if desired, and serve. 

Source: [Moosewood Cooks at Home](http://www.moosewoodcooks.com/products-page/product-category/moosewood-restaurant-cooks-at-home/)
