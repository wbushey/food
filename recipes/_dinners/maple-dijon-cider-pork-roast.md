---
title: Maple-Dijon-Cider Pork Roast
ingredients:
  pork loin: 2 lbs
  maple syrup: 2 tbsp
  dijon mustard: 4 tsp
  cider: 24 oz apple or pear
  carrots: 6
  potatos: 1.5 lb red and gold
  apples: 3 green
  salt:
  black pepper:
  garlic powder:
  thyme:
  water: 4 tbsp
  corn starch: 1 tbsp
tags:
  - pork
  - gluten-free
time: 2.5 hours
servings: 8
---

1. Preheat oven to 325 degrees.
2. Pat pork dry and paper towels then season all over with salt, pepper, garlic powder, and thyme.
3. Heat a thin layer of high heat oil in dutch oven over medium-high heat. Sear pork for 2 minutes on all sides, until golden brown. Transfer pork to plate.
4. Chop carrots into ~1 inch long pieces, chop potatos and apples into medium/large pieces. Place some into dutch oven, then the pork loin, then the remaining vegetables/apples.
5. Place dutch oven in oven. Let bake until pork reaches internal temperature of 145F, approximately 1.5 hours.
6. Remove dutch oven from oven. Let pork rest on cutting board. Transfer vegetables/apples to large bowl.
7. Heat dutch oven over medium-high heat. Pour in cider, maple syrup, and mustard. Scrape browned bits off bottom and mix to combine. Let simmer until sauce is slightly reduced, ~5 minutes.
8. Stir together water and corn starch in a cup. Pour mixtured into sauce and combine. Continue to simmer and occassionally mix until sauce is thickened; ~4 minutes.
9. Slice pork.
10. Serve pork with vegetables/apples and sauce.

[Based on this pork tenderloin recipe](https://iowagirleats.com/maple-dijon-cider-pork-tenderloin/)
