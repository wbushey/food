---
title: Breakfast for Dinner 
ingredients:
  bacon: optional
  eggs: optional
  pancakes: optional
  bread: optional
tags:
  - gluten-free
time: 30 - 40 minutes
servings: 2
---

1. Make breakfast
2. Eat it for dinner!
