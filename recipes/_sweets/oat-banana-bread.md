---
title: Oat Banana Bread
ingredients:
  oat flour: 2 cups
  baking soda: 1 teaspoon
  salt: 1/4 teaspoon
  butter: 1/2 cup - melted
  brown sugar: 3/4 cup
  eggs: 2
  bananas: 4 - overripe
tags:
  - gluten-free
time: 65 minutes
servings: 10
---

1. Preheat over to 350 degrees F. Lightly grease a 9x5 inch loaf pan.
2. If neccessary, grind oats into oat flour.
3. In a large bowl, combine flour, baking soda and salt.
4. In a seperate bowl, cream together butter and brown sugar. Stir in eggs. Add
   bananas, mash, and stir.
5. Stir banana mixture into flour mixture; stir just to moisten. Pour batter
   into loaf pan.
6. Bake in oven for 50 to 60 minutus, until a toothpick inserted into center of
   the loaf comes out clean.
7. Let bread cool in pan for 10 minutes.
