---
title: Banana Bread
ingredients:
  rice flour: 1 1/4 cup
  potato starch: heaping 1/3 cup
  tapioca flour: heaping 1/4 cup
  xanthan gum: 2/3 tsp
  baking soda: 1 teaspoon
  salt: 1/4 teaspoon
  eggs: 4
  bananas: 2 cups or 4-5 medium
  sugar: 1 cup
  unsweetened applesauce: 1/2 cup
  canola oil: 1/3 cup
  vanilla extract: 1 teaspoon
  walnuts: 1/2 cup, chopped
tags:
  - gluten-free
time: 65 minutes
servings: 24
---
0. Preheat oven to 350
1. In a large bowl, combine the flour, baking soda and salt. In a small bowl, whisk the eggs, mashed bananas, sugar, applesauce, oil and vanilla. Stir into dry ingredients just until moistened.
2. Transfer to two 8-in. x 4-in. loaf pans coated with cooking spray. Sprinkle with walnuts. Bake at 350° for 45-55 minutes or until a toothpick inserted near the center comes out clean. Cool for 10 minutes before removing from pans to wire racks. Yield: 2 loaves (12 slices each).

Notes: Also works as muffins! And you can add chocolate too. I replaced the 2 cups gf flour mix with roughly what I use.

[Source](http://www.tasteofhome.com/recipes/gluten-free-banana-bread)
