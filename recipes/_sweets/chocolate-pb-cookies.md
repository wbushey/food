---
title: Chocolate Peanut Butter Cookies
ingredients:
  creamy peanut butter: 1 heaping cup
  light or dark brown sugar: 1 cup packed
  egg: 1 large
  vanilla extract: 1 tablespoon
  unsweetened natural cocoa powder: 1/2 cup
  baking soda: 1 teaspoon
tags:
  - gluten-free
time: 20 minutes
servings: 13
---

1. To the mixing bowl of a stand mixer fitted with the paddle attachment, combine peanut butter, brown sugar, egg, vanilla, and beat on medium-high speed until well-combined and the sugar is fully incorporated and is mixture is no longer gritty or granular, about 5 minutes. Stop to scrape down the bowl as necessary. Note regarding peanut butter - although natural peanut butter or homemade peanut butter may work, I recommend using storebought peanut butter like Jif, Skippy, Peter Pan or similar so that cookies bake up thicker and spread less. Using natural or homemade peanut butter tends to result in thinner, flatter cookies, that are prone to spreading.
2. Add the cocoa powder (I don't bother to sift), baking soda, and beat to incorporate, 1 to 2 minutes. Dough may be a bit crumbly in pieces, but pieces should all stick together forming a large mound when pinched, squeezed, and pushed together. If your dough seems dry, adding 1 to 2 additional tablespoons of peanut butter will help it combine.
3. Using a 2-inch medium cookie scoop (about 2 heaping tablespoons of dough or 1.80 ounces by weight), form dough mounds or roll dough into balls. Recipe makes 13 cookies; dividing dough into 13 equal portions is another way to do this.
4. Place dough on a large plate and flatten each mound with a fork, making a criss-cross pattern on top. Slightly flattening the mounds before baking ensures they don't stay too domed and puffed while baking because this dough, when properly chilled, doesn't spread much; just don't over-flatten. Cover plate with plastic wrap and refrigerate for at least 2 hours, or up to 5 days, before baking. Do not bake with warm dough.
5. Preheat oven to 350F, line 2 baking sheets with Silpat Non-Stick Baking Mats, parchment, or spray with cooking spray; set aside. Space dough 2 inches apart (8 to 10 per tray) and bake for 8 to 10 minutes, until edges are set and tops are barely set, even if slightly underbaked in the center. It's tricky to discern if they're done or not because they're so dark, but watch them very closely after 7 minutes. I recommend the lower end of the baking range. Cookies firm up as they cool, and baking too long will result in cookies that set up too crisp and hard (The cookies shown in the photos were baked for 8 minutes, with trays rotated at the 4-minute mark, and have chewy edges with pillowy, soft centers).
6. Allow cookies to cool on the baking sheet for 5 to 10 minutes before removing and transferring to a rack to finish cooling. Store cookies in an airtight container at room temperature for up to 1 week, or in the freezer for up to 3 months. Alternatively, unbaked cookie dough can be stored in an airtight container in the refrigerator for up to 5 days, so consider baking only as many cookies as desired and save the remaining dough to be baked in the future when desired.

Notes: works best when not using natural peanut butter

[Source](http://www.averiecooks.com/2013/02/thick-and-soft-chocolate-peanut-butter-cookies.html)
