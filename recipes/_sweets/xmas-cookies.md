---
title: Cut-Out Sugar Cookies
ingredients:
  butter or shortening or coconut oil: 1/2 cup (1 stick)
  sugar: 3/4 cup
  egg: 1 large
  vanilla: 1/2 teaspoon
  rice flour: 1 1/4 cups
  potato starch: 1/2 cup
  tapioca starch: 1/4 cup
  baking powder: 1/2teaspoon
  xanthan gum: 1/4 teaspoon
tags:
  - gluten-free
time:
servings:
---

1. Cream together the butter and sugar until light in color.
2. Add the egg and vanilla and beat until combined.
3. In a small bowl, whisk together the rice flour, potato starch, tapioca starch, baking powder, and xanthan gum. Gradually add to the mixing bowl with the mixer on low.
4. Increase the mixer speed and beat until it starts to pull away from the sides of the bowl.
5. Wrap the dough in wax paper or plastic wrap and flatten it out some. Refrigerate at least 2 hours.
6. Preheat oven to 350°. Line two baking sheets with parchment paper.
7. Working with half of the dough at a time, place the chilled dough on a silicone mat. Cover with a piece of plastic wrap and roll to 1/4 inch thick.
8. Remove the plastic wrap and use cookie cutters to cut shapes from the dough. Carefully transfer with a spatula to a lined baking sheet.
9. Gather the scraps and reroll. If dough has warmed too much and become too soft and sticky to handle, return to the refrigerator to cool.
10. Bake one cookie sheet at a time for about 10 minutes or until the edges are just starting to brown. Remove from oven and cool for 2 minutes on the baking sheet, then transfer to a wire cooling rack.

Notes: There are other holiday cookie recipes [here](http://glutenfreehomemaker.com/gluten-free-christmas-recipes-2/) that I haven't tried! Also, 
add colored butter cream frosting to decorate: mix 3 cups powdered sugar, 1 cup butter (room temperature), 1 tsp vanilla, 1-2 tsp milk/nondairy milk/whipping cream; separate and color.

[Source](http://glutenfreehomemaker.com/sugar-cookies-cut-out/)
