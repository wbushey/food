---
title: Chocolate Gingerbread
ingredients:
  sorghum flour: 1 cup
  almond meal: 3/4 cup
  corn starch, sweet rice flour, or potato starch: 1/2 cup
  light brown sugar: 1 cup
  unsweetened cocoa powder: 1/3 cup
  baking powder: 2 tsps
  baking soda: 3/4 tsp
  xanthan gum: 1 tsp
  ground ginger: 2 tsps
  ground cinnamon: 1 tsp
  fine sea salt: 3/4 tsp
  nutmeg or cardamom: 1/2 tsp
  egg: 2 free range
  unsulphured molasses: 1/2 cup
  organic coconut oil: 1/4 cup
  non dairy milk: 4 tbsps
  vanilla extract: 2 tsps
tags:
  - gluten-free
time: 60 minutes
servings: 10
---

1. Preheat the oven to 350ºF. Line a 9-inch ceramic loaf pan with a piece of parchment paper that rises up the long sides of the pan.
2. In a mixing bowl, whisk together the dry ingredients- flours/starches through nutmeg.
3. Add in the eggs, molasses, coconut oil, non-dairy milk and vanilla extract and beat well, until the batter is smooth. A standing mixer will handle this task best. 
4. Scoop the gingerbread batter into the prepared loaf pan and bake in the center of the oven for roughly an hour. The top will crack a bit. A cake tester inserted into the center should emerge clean.
5. Allow the loaf to cool in the pan until it is cooled enough to handle. Gently remove it from the pan (this is where the parchment paper lining comes in handy) and continue to cool on a wire rack.
6. Slice with a sharp bread knife.
7. Wrap the loaf well for storing overnight. For longer storage, slice and wrap pieces in foil, bag, and freeze.

Notes: don't use potato flour

[Source](http://www.yummly.com/recipe/external/Karina_s-Gluten-Free-Chocolate-Gingerbread-577104)
