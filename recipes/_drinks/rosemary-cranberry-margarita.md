---
title: Rosemary Cranberry Margarita
ingredients:
  tequila: 1.5 oz blanco
  cranberry juice: 0.75 oz unsweetened
  orange liqueur: 0.5 oz
  rosemary simple syrup: 0.5 oz
  lime juice: 0.5 oz
  sugar: 2 tsp
  salt: 1 tsp
  rosemary sprig: 1
  cranberries: 3
tags:
  - gluten-free
servings: 1
---

[Recipe for Rosemary Simple Syrup](/mixes/rosemary-simple-syrup.html)

1. Prepare sugar/salt for glass rim: mix sugar and salt on a small plate. Using fingers, rub rosemary leaves into mixture until oil releases; about 30 seconds.
2. Wet glass rim with lime wedge and rim glass with sugar/salt mixture.
3. In shaker combine tequilla, cranberry juice, orange liqueur, lime juice, rosemary simple syrup, and ice. Shake until well chilled.
4. Strain into glass. Garnish with rosemary sprig and cranberries.

[Source](https://www.foodandwine.com/rosemary-cranberry-margarita-recipe-8725243)
