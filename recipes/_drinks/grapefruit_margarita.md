---
title: Grapefruit Margarita
ingredients:
  tequila: 1.5oz, blanco
  grapefruit crema: 1.5 oz
  grapefruit juice: 1.5 oz (1/4 of a grapefruit)
  lime: 1/8, or a teaspoon lime juice
  salt:
tags:
  - gluten-free
servings: 1
---

1. If you like a salted glass, salt the serving glass.
2. Put ice in serving glass.
3. Combine teguila, grapefruit crema, grapefruit juice, lime juice, and ice in a shake. Shake for ~10 seconds.
4. Pour into serving glass. If glass isn't salted, add a pinch of salt.
