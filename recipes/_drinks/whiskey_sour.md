---
title: Whiskey Sour
ingredients:
  bourbon: 1.5 oz
  simple syrup: .5 oz
  lemon juice: 1 wedge or .5oz
  Marashino cherry: optional
tags:
  - gluten-free
---

*Note*: If substituting simple syrup with honey, do not use ice.

1. Combine bourbon, lemon juice, simple syrup, and ice in shaker. Shake vigorously until very cold; about 20 seconds.
2. Strain cocktail into glass. Garnish with cherry.
