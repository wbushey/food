---
title: Chocolate Creamsicle
ingredients:
  Triple Sec: 1oz
  Irish Cream: 1oz
  Creme de Cacao: 1oz
  chocolate syrup: 1 tbsp
tags:
  - gluten-free
---

1. Combine Triple Sec, Irish Cream, and Creme de Cacao with ice in shaker and shake.
2. Pour into glass
3. Add chocolate syrup and stir
