---
title: Cream of Coconut
ingredients:
  coconut milk: 13.5oz (400ml) can full fat
  sugar: 1 cup
  salt: pinch
  coconut power: 1 tbsp, optional
tags:
  - gluten-free
---

1. Combine all ingredients in a saucepan. Gently heat on low and stir until sugar is dissolved.

Based on [this recipe](https://www.artofdrink.com/ingredient/cream-of-coconut-recipe).
