---
title: Old Fashioned
ingredients:
  bourbon: 1.5 oz
  simple syrup: 1 or 2 tsp
  angostura bitters: 1 dash
  orange bitters: 1 dash
  water: 1 tsp
  orange: optional 1 rind slice
  Maraschino cherry: optional
tags:
  - gluten-free
---

*Maple Alternative*: Substitute simple syrup with maple syrup

1. Combine syrup, water, and bitters into a whiskey glass. Stir to combine.
2. Add ice cube.
3. Pour in bourbon.
4. Garnish with orange rind and cherry.
