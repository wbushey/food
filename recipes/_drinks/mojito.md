---
title: Mojito
ingredients:
  rum: 1.5oz, white
  mint: 6 to 8 leaves
  simple syrup: 4 teaspoons
  lime: 1/2, or a tablespoon lime juice
  club soda: 3oz
tags:
  - gluten-free
servings: 1
---

1. In the glass, muddle the mint leaves and simple syrup
2. Add lime juice. If using a lime, add the lime half after juicing it.
3. Add rum and stir
4. Top off with ice and club soda
5. Garnish with a sprig of mint
