---
title: Coquito
ingredients:
  cream of coconut: 13.5oz (400ml) Coco López or home made
  evaporated milk: 12 oz
  cinnamon sticks: 4
  whole cloves: 3
  star anise: 2
  vanilla extract: 1 tsp
tags:
  - gluten-free
servings: 10
---

[Home made Cream of Coconut](/drinks/cream_of_coconut).

1. Break cinnamon sticks in half.
2. Mix all ingredients in a saucepan, simmer for about 10 minutes.
2. Strain into a bottle and let cool.

## Alcohol

Serve 2 1/2 parts Coquito with 1 part rum (Don Q Gold suggested).

Based on [How to Drink recipe](https://www.youtube.com/watch?v=-xMhXHj6yXU).
