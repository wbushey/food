---
title: El Presidente
ingredients:
  dark rum: 1.5 oz
  triple sec: .75 oz
  dry vermouth: .75 oz
  grenadine: dash
tags:
  - gluten-free
servings: 1
---

1. Mix all ingredients in a shaker. Strain and pour into a chilled glass.
