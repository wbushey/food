---
title: Pumpkin Pie Spice
ingredients:
  cinnamon: 1 Tbst and 2 tsp
  nutmeg: 1 tsp and 1/4 tsp
  ginger: 1 tsp and 1/4 tsp
  cloves: 1/2 tsp and 1/8 tsp
tags:
  - gluten-free
time: 5 min
servings: 10 (20 tsp)
---

1. Mix all ingredients, store in airtight container

Note: source has a calculator to adjust amounts

[Source](https://www.allrecipes.com/recipe/20476/pumpkin-spice/)