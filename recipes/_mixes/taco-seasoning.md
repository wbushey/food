---
title: Taco Seasoning
ingredients:
  chili powder: 1 Tbsp
  garlic powder: 1/4 tsp
  onion powder: 1/4 tsp
  crushed red pepper flakes: 1/4 tsp
  dried oregano: 1/4 tsp
  paprika: 1/2 tsp
  ground cumin: 1 1/2 tsp
  salt: 1 tsp
  pepper: 1 tsp
tags:
  - gluten-free
time: 5 min
servings: 10 (1 oz)
---

1. Mix all ingredients, store in airtight container

Note: source has a calculator to adjust amounts

[Source](https://www.allrecipes.com/recipe/46653/taco-seasoning-i/)