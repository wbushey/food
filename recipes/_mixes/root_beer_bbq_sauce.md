---
title: Root Beer BBQ Sauce
ingredients:
  root beer: 1 cup to 1 bottle
  ketchup: 1 cup
  lemon juice: 1/4 cup (half a lemon)
  Worcestershire Sauce (gluten-free): 3 tbsp
  light brown sugar: 3 tbsp
  ginger: 1/2 tsp ground -or- 1 in minced fresh
  garlic: 1/2 tsp powder -or- 2 cloves minced
  onion: 1/2 tsp powder -or- 1/4 onion chopped
  salt: to taste
  pepper: to taste
tags:
  - gluten-free
time: 1 hr
servings: 4
---

 1. Mince and chop ginger, garlic, onion if using fresh
 2. Combine all ingredients in a medium saucepan
 3. Bring to a boil, stirring occasionally. Once boiling, reduce to simmer until sauce is reduced by half (about 20 minutes)

[Source](https://www.mrfood.com/chicken/root-beer-chicken)
