---
title: Savory Chicken Marinade
ingredients:
  olive oil: 1/4 cup
  balsamic vinegar: 1/4 cup
  soy sauce: 1/4 cup
  Worcestershire sauce: 1/8 cup
  lemon juice: 1/16 cup
  brown sugar: 1/4 cup
  rosemary: 1 tsp
  Dijon mustard: 1 tbsp
  salt: 1 tsp
  black pepper: 1/2 tsp
  Italian spices: 2 tsp
  garlic powder: 1 tsp
tags:
  - gluten-free
time: 4 hours to 24 hours
servings: 4
---
Measurements are for 3 chicken breasts, about 1.5 lbs. Adjust based on actual amount of chicken.

1. Combine all ingredients in a bowl and whisk together.
2. Place chicken in a ziploc bag. Pour marinade over chicken. Close bag.
3. Marinade for between 4 and 24 hours.

Based on [this](https://www.momontimeout.com/the-best-chicken-marinade-recipe/), [this](https://www.modernhoney.com/the-best-chicken-marinade-recipe/), and [this](https://www.spendwithpennies.com/the-best-chicken-marinade/).
