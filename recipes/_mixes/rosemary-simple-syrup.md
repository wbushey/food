---
title: Rosemary Simple Syrup
ingredients:
  sugar: 3/4 cup
  water: 1/2 cup
  rosemary sprigs: 3
tags:
  - gluten-free
servings: 1/2 cup
---

1. Stir together sugar, water, and rosemary in a small saucepan
2. Bring to a gentle simmer over medium-low heat. Stir occasionally until sugar is disolved into water. Simmer for ~8 minutes.
3. Remove from heat, let cool completely, ~20 minutes.
4. Strain into storage container. Refrigerate.
