---
title: Cilantro Lime Rice
ingredients:
  rice: 1 cup, long-grain white
  cilantro: 1/2 cup, fresh
  lime: 1
  olive oil: 1 tbsp
  salt: 
tags:
  - vegetarian
  - gluten-free
time: 30 minutes
servings: 3
---

1. Cook Rice
2. Rinse and chop cilantro
3. Mix together olive oil, cilantro, zest from lime, juice from lime, and 2 tbsp water.
4. When rice is cooked, mix in remaining ingredients.
