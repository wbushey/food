---
title: Vegetable Bhaji
ingredients:
  potatoes: 4 large
  cauliflower: 1 cup florets
  carrot: 1
  green peas: 1/2 cup
  green chile peppers: 4
  onion: 1 large
  garlic: 3 cloves
  ginger root: 2 tsp
  cumin seed: 1 tsp
  black mustard seed: 1 tsp
  curry powder: 1 tsp
  cumin: 1 tsp
  bay leaves: 2
  cooking oil: 2 tbsp
  chili powder: optional - 1/2 tsp
  cilantro: optional
tags:
  - vegetarian
  - gluten-free
time: 90 minutes
servings: 8
---

1. Finely chop potatoes and carrot. Chop onion, garlic, chiles (and cilantro). Cut florets from cauliflower.
2. Partially steam the potatoes, cauliflower, carrot, and peas until they start to soften; ~ 13 minutes. Drain vegetables in colander.
3. Heat cooking oil in large pot over medium heat. Cook onion until golden; ~ 10 minutes. Stir in cumin seed, mustard seed, and bay leaves. Cook and stir until seeds begin to sputter; ~ 30 seconds. Add chiles and garlic; grate in ginger. Cook and stir until garlic begins to brown; about 1 minute.
4. Stir in ground cumin, curry powder, (and chili powder). Add potatoes, cauliflower, carrot, and peas. Cook and stir on medium-low heat until vegetables are tender and coated with spices; ~ 30 minutes.
5. Serve (with cilantro).

[Source](https://www.allrecipes.com/recipe/93896/indian-vegetable-bhaji/)
