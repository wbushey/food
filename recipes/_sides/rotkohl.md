---
title: Rotkohl
ingredients:
  cabbage: 1 medium head
  onions: 2 medium
  apples: 3 granny smith
  garlic: 1 clove
  peppercorns: 1/2 tsp
  cloves: 2
  bay leaves: 2
  butter: 2 tbsp
  red wine: 1 cup dry
  red wine vinegar: 2 tbsp
  salt: 1 1/2 tsp
  pepper: 1/4 tsp
tags:
  - vegetarian
  - gluten-free
time: 50 minutes
servings: 8
---

1. Chop onions and garlic.
2. Saute onions and garlic in butter in a large pot or dutch oven.
3. Chop  apples. Put bay leaves, cloves, and peppercorns in cheesecloth packet.
4. Deglaze pot with wine; scrape bottom of pot if needed.
5. Add apples, salt, pepper, and spice packet. Shred cabbage into pot.
6. Bring to a boil, then reduce heat. Cover and simmer for 35 minutes (until cabbage is tender.), stirring occasionally.
7. Stir in red wine vinegar. Remove spice packet.
8. Serve hot or cold.

[Source](https://www.tasteofhome.com/recipes/rotkohl-red-cabbage/)
