---
title: Grilled Broccoli
ingredients:
  broccoli: 1 or 2 crowns
  lemon juice: 2.5 tbps
  olive oil: 2tbps
  salt: 1/4 tsp
  pepper: 1/4 tsp
tags:
  - vegetarian
  - gluten-free
time: 30 minutes + marinade time.
servings: 2
---

1. Combine lemon juice, olive oil, salt, and pepper in a bowl.
2. Chop broccoli, add to bowl and toss.
3. Refridgerate for at least 30 minutes.
4. Grill over medium-low heat for 5 minutes each side, then over high for some char.
