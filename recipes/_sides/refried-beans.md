---
title: Refried Beans- Instant Pot
ingredients:
  beans: 1 lb/2 cups dried black or pinto
  Extra Virgin Olive Oil: 2 tsp
  onion: 1 small yellow
  jalapeno: 1 cored, diced
  garlic: 3 cloves minced
  chicken/veg stock: 4 cups
  water: 3 cups
  bay leaves: 2
  salt: 1 1/2 tsp
  ground cumin: 1 tsp
  oregano: 1 tsp dried
  cayenne pepper: dash
tags:
  - vegetarian
  - gluten-free
time: 1:45
servings: 6 cups
---

1. Thoroughly rinse beans and set aside
2. Turn on Instant Pot to saute, add oil. Once hot, add onion, jalapeno. Saute for 2 min, then add garlic and cook until fragrant- 30 sec. Add splash of chicken stock and scrape bottom of pan. Add remaining stock, water, bay leaves, salt, cumin, oregano, cayenee and beans. Stir to comobine.
3. Seal Instant Pot and pressure cook on high for 45 min. Let pressure release naturally 25 min, then vent. Beans will be liquidy.
4. Discard bay leaves. Reserve 2 cups cooking liquid, drain remainder. Return beans to pot and use immersion blender to puree. Add reserved liquid as desired.
5. Taste and adjust seasoning as desired.

Notes: Leftover beans freeze well in a ziploc but let them cool completely before freezing. Can add water/stock when thawing and warming. If you forget to drain some liquid, you can put the pot on saute and cook off the liquid but it takes a while. If you are making this with pulled chicken, it's easy to cook generic poached chicken on the stovetop and pull apart.

[Source](https://www.wellplated.com/instant-pot-refried-beans/#wprm-recipe-container-32677)
