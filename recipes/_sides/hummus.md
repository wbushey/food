---
title: Hummus
ingredients:
  chickpeas: 1 (15-ounce) can or 1 1/2 cups (250 grams) cooked
  lemon juice: 1/4 cup (60 ml)(1 large lemon)
  tahini: 1/4 cup (60 ml)
  garlic: 1 clove minced
  EVOO: 2 Tbsp
  Ground cumin: 1/2 tsp
  salt: 1/2 tsp
  water: 2-3 Tbsp
  Paprika or sumac: dash
tags:
  - vegetarian
  - gluten-free
time: 20 min
servings: 6
---

1. Make tahini: toast 1/4 cup sesame seeds in skillet over medium heat, grind in spice/coffe grinder. Add olive oil until a smooth paste is formed.
2. Combine tahini and lemon juice in food processor and process for 1 minute, scrape sides and bottom of the bowl then process for 30 seconds more.
3. Add olive oil, minced garlic, cumin, and a 1/2 teaspoon of salt to the whipped tahini and lemon juice.
4. Open, drain, and rinse chickpeas. Add half of the chickpeas to the food processor and process for 1 minute. Add remaining chickpeas and process until thick and smooth; 1 to 2 minutes.
5. Add water to get right consistency, process.
6. Top with olive oil, paprika/sumac to serve.
   
Note: fresh lemon tastes better. More garlic is good, but not too much. Could add other flavors- roasted red pepper, basil, etc.

[Source](https://www.inspiredtaste.net/15938/easy-and-smooth-hummus-recipe/#itr-recipe-15938)
