---
title: Quinoa Tabbouleh
ingredients:
  quinoa: 1 cup rinsed
  salt: 1/2 tsp
  lemon juice: 2 Tbsp fresh
  garlic: 1 clove minced
  EVOO: 1/2 cup
  English or Persian cucumber: 1 large
  cherry tomatoes: 1 pint halved
  flat leaf parsley: 2/3 chopped
  fresh mint: 1/2 cup
  scallions: 2 thinly sliced
  salt and pepper: some
tags:
  - vegetarian
  - gluten-free
time: 20 min + chill time
servings: 6
---

1. Bring quinoa, 1/2 tsp salt, 1/14 cups water to boil in pot over high heat. Reduce to med-low, cover, simmer until quinoa is tender- 1bout 10 min. Remove from heat and let stand 5 min. Fluff with fork. Cool quinoa- spread out on baking sheet (helps remove excess water).
2. Whisk lemon juice, garlic in small bowl. Gradually whisk in olive oil. Season with salt and pepper.
3. Put quinoa into large bowl, add 1/4 cup dressing. Chill. 
4. Add chopped cucumber, tomatoes, herbs, scallions and toss to coat. Season with salt and pepper. Drizzle remaining dressing over top.

Notes: Chill step can be done day ahead; good to at least let it sit a bit but not necessary. If you don't have or like tomatoes, could use red bell peppers.

[Source](https://www.epicurious.com/recipes/food/views/quinoa-tabbouleh-395939)