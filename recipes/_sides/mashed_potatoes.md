---
title: Garlic Mashed Potatoes
ingredients:
  potatoes: 3 lbs Yukon gold
  garlic: 4 cloves
  garlic salt: to taste
  pepper: to taste
tags:
  - vegetarian
  - gluten-free
time: 50 minutes
servings: 4
---

For slightly fluffy mashed potatoes, uses a mix of Yukon gold and Russet potatoes.

1. Peel and cube potatoes
2. Place potatoes in pot, cover with water, salt water
3. Boil potatoes for 20 minutes
4. Mince garlic
5. Sauté garlic
6. Once potatoes are tender, use slotted spoon to remove potatoes to a bowl. Reserve potato water.
7. Mash potatoes
8. Add and mix garlic, garlic salt, pepper, and 3/4 cup potato water in to potatoes.
