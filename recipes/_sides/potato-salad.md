---
title: Potato Salad
ingredients:
  potatoes: 2 lbs yukon gold or red
  celery: 2 stalks
  mayo: 1 cup
  red onion: 1 small
  dill: 1/4 cup fresh chopped
  vinegar: 1-2 Tbsp
  lemon juice: 1/8 cup (1 half lemon)
  Dijon mustard: 1 Tbsp
  salt: 3/4 tsp
  pepper: to taste
tags:
  - vegetarian
  - gluten-free
time: 40 min
servings: 12
---

1. Put potatoes in pot, cover 1 inch. Salt and bring to boil, cook until tender- 25 min.
2. Drain potatoes and leave in uncovered pot to cool (helps get rid of excess water)
3. Dice celery, stir with 3/4 tsp salt and the remaining ingredients in bowl large enough to hold potatoes
4. When potatoes are cool, cut into 1 inch peices and add to bowl. Stir gently to coat with dressing. Add more salt and pepper to taste.
   
Notes: I like 1 Tbsp vinegar. I leave potato skins on because I like them; original recipe says to peel. I use the whole thing of dill from the store because what else would I do with dill?

[Source](https://www.foodnetwork.com/recipes/dave-lieberman/creamy-dijon-dill-potato-salad-recipe-1916486)
