---
title: Stove Top Rice
ingredients:
  rice: 1 cup
  water: 1.5 cups
  butter: 1 tsp
  salt: 
tags:
  - vegetarian
  - gluten-free
time: 30 minutes
servings: 3
---

1. Rinse rice
2. Bring water, salt, and butter to a boil
3. Stir in rice and return to boil.
4. Reduce heat to a simmer, cover, cook rice for 16 to 18 minutes.
5. Remove rice from heat, let steam for 10 minutes.
