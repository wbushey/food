---
title: Peach Salsa
ingredients:
  peaches: 3 medium size
  pineapple: 1/2 cup chopped
  red bell pepper: 1
  red onion: 1 small (~ 1/4 cup chopped)
  green onion: 3
  cilantro: 1/2 bunch
  lime juice: 2 tbsp (~ half fresh lime)
  chili powder: 1/4 tsp
  salt: 1/2 tsp
  
tags:
  - gluten-free
  - vegetarian
time: 10 minutes
services: 4
---

1. Wash peaches, pepper, onions, cilantro.
2. Pit and dice peaches. Dice pepper and onions, chop cilantro. Squeeze lime juice.
3.  In large mixing bowl combine peaches, pineapple, red bell pepper, red onion, green onion, cilantro. Toss to combine.
4. Add lime juice and chili powder. Toss to combine. Taste and season with salt and pepper. Add more lime juice, chili powder, and salt to taste.

[Source](https://juliasalbum.com/chicken-with-peach-salsa/)
