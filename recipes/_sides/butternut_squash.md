---
title: Butternut Squash with Onions and Pecans
ingredients:
  butternut squash: 1 small
  onion: 1
  pecans: 1 cup chopped
  parsley: 3 tablespoons
  butter: 3 tbsp
  salt: to taste
  pepper: to taste
tags:
  - vegetarian
  - gluten-free
time: 2 hours
servings: 6
---

1. Preheat oven to 350
2. Chop pecans and place on ungreased baking sheet. Toast in oven for 5 to 8 minutes.
3. Spit and peel squash - use knife to peel skin. Remove seeds and toaste in oven for 20 minutes.
4. Cut squash into 1/2 inch cubes.
5. Chop onion and saute in butter.
6. Add squash to onions. Cover and cook until squash is tender, about 15 minutes. Stir frequently.
7. Chop parsley.
8. Season squash with salt and pepper. Mix in pecans and parsley.
