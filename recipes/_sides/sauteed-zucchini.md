---
title: Sautéed Zucchini
ingredients:
  zucchini: 2
  garlic: 2 cloves
  salt:
  olive oil:
tags:
  - vegetarian
  - gluten-free
time: 10 minutes
servings: 2
---

 1. Cut zucchini into 1/2" slices. Mince garlic.
 2. Heat pan and oil to medium-high.
 3. Add zucchini and salt. Cook and stir until zucchini begins to caramelize, about 5 to 7 minutes.
 4. Add garlic and cook for 1 more minute.

[Source](https://www.washingtonpost.com/news/voraciously/wp/2020/05/12/chocolate-milk-simmered-chicken-dont-knock-it-until-you-try-it/)
