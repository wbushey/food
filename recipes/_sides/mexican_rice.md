---
title: Mexican Rice
ingredients:
  long grain white rice: 1 1/4 cups
  broth: 1 1/2 cups chicken or vegetable
  tomato paste: 1 tbsp
  onion powder: 1/2 tsp
  garlic powder: 1/2 tsp
  cumin: 1/2 tsp
  chili powder: 1/4 tsp
  black pepper: 1/4 tsp
  salt: 1/4 tsp
  oil: 1 1/2 tbsp
tags:
  - vegetarian
  - gluten-free
time: 30 minutes
servings: 4
---

1. Make broth if needed.
2. Heat deep pot and oil over medium high heat. Add rice and stir constantly until rice begins to turn golden brown.
3. Add broth; it will bubble up. Immediately add tomato paste, onion powder, garlic powder, cumin, chili powder, black pepper, and salt. Stir to dissovle tomato paste.
4. Bring to a boil. Cover and simmer for 20 minutes.
5. Fluff rice with a fork once it is done.
