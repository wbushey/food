// Reference to all the checkboxes in the filter menu
const checkboxes = $('.menu.filter input[type="checkbox"]');

// Reference to all of the recipe list items
const all_recipes = $('ul.recipes li.recipe');

function only_show_recipes_with_tags(tags){
    // Hide all of the recipes
    all_recipes.hide();

    // Find all of the recipes to show;
    var recipes_to_show = $();
    tags.forEach(tag => 
      recipes_to_show = recipes_to_show.add(all_recipes.filter(`[data-tags~=${tag}]`))
    );

    recipes_to_show.show();
}

// On load, add listeners to all checkboxes in the filter menu
checkboxes.click(function() {
  // Get tags that are checked
  var checked_tags = checkboxes
    .filter(':checked')
    .map(function() { return this.id})
    .get();

  if (checked_tags.length == 0){
    all_recipes.show();
  } else {
    only_show_recipes_with_tags(checked_tags);
  }
});
